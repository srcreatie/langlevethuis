function setData(callback) {

    var devDynamicContent = {};

    devDynamicContent.srFeed = [{}];
    devDynamicContent.srFeed[0]._id = 0;
    devDynamicContent.srFeed[0].isWorking = "Default feed 2";
    devDynamicContent.srFeed[0].exit_url = "";
    devDynamicContent.srFeed[0].copy = {};
    devDynamicContent.srFeed[0].copy.h1 = "Je eigen hijskraan morgen in huis, kopen kopen kopen!"
    devDynamicContent.srFeed[0].color = {};
    devDynamicContent.srFeed[0].style = {};
    devDynamicContent.srFeed[0].style.headlineColor = "#0000FF" ; 
    devDynamicContent.srFeed[0].background300x250 = dimension("example-300x250.png", "300x250");
    devDynamicContent.srFeed[0].background300x600 = dimension("example-300x600.png", "300x600");

    return devDynamicContent;
}

module.exports = setData;