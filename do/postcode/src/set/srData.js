var setVersion = require('feed');

function setup(callback) {

    if (srBanner.debug) {

        console.log("%c ==============feed=============", 'background: #0060a1; color: #FFFFFF');
        console.log("USING LOCAL FEED");
        console.log("%c _______________________________", 'background: #0060a1; color: #FFFFFF');

    }

    var devDynamicContent = {};

    var devDynamicContent = setVersion();

    var dd = devDynamicContent.srFeed[0];

    callback(dd);

}

module.exports.setup = setup;