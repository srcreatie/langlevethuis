function setElements(callback) {

    config = {};
    config.bannerWidth = 160;
    config.bannerHeight = 600;

    if (Enabler.isInitialized()) {
        enablerInitHandler();
    } else {
        Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler);
    }

    function enablerInitHandler() {

        if (Enabler.isPageLoaded()) {
            pageLoadedHandler();
        } else {

            Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED,
                pageLoadedHandler);
        }
    }

    function pageLoadedHandler() {

        addClicktag(dd.exit_url);
        loadFont();

        function loadFont() {
            sr.font.add([asset('bold.woff'),
                asset('semibold.woff')
            ], add);
        }

        function add() {

            ___("bg")
                .image(asset("bg-160x600.jpg"), { width:160, height:600, fit:true })

            ___("pink")
                .style({ background:"#D92266", css:"padding:11px" })
                .position({ left:14, top:14 })

                ___("pink>h1")
                    .text("Zorgeloos besparen", { fontSize:20, color:"#fff", width:110, webfont:"bold" })
                    .style({ css:"position:relative;" })

                ___("pink>h2")
                    .text("met de zon als bron", { fontSize:20, color:"#fff", width:110, webfont:"semibold" })
                    .style({ css:"position:relative;" })

                    ___("h3")
                        .text("Doe de zonnepanelen-<br />check!", { fontSize:19, width:100, color:"#fff", webfont:"bold" })
                        .position({ left:10, top:14 })
                    
                    ___("h4")
                        .text("Ontdek binnen één minuut hoeveel je kunt besparen", { fontSize:19, width:140, color:"#fff", webfont:"semibold" })
                        .position({ left:10, push:{ el:__("h3"), bottom:5 } })

                    ___("h5")
                        .text("Vul je postcode in en ontvang persoonlijk advies", { fontSize:19, width:140, color:"#fff", webfont:"semibold" })
                        .position({ left:10, push:{ el:__("h3"), bottom:5 } })

            ___("zipcode")
                .style({ greensock:{ alpha:0, scale:0 } })

            ___("number")    
                .style({ greensock:{ alpha:0, scale:0 } })

            ___("zipcode_label")
                .text("Postcode", { fontSize:11, color:"#fff", webfont:"semibold" })
                .style({ greensock:{ alpha:0, scale:0 } })
                .position({ left:10, top:219 })

            ___("number_label")
                .text("Huisnr. & toev.", { fontSize:11, color:"#fff", webfont:"semibold" })
                .style({ greensock:{ alpha:0, scale:0 } })
                .position({ left:10, top:278 })

            ___("footer")
                .style({ width:160, height:140, background:"#fff" })
                .position({ bottom:0, left:0 })

                ___("cta")
                    .text("Bereken besparing", { webfont:"bold", fontSize:13, color:"#fff", textAlign:"center" })
                    .style({ background:"#02B5CE", css:"border-radius:3px; padding:7px 11px; border-bottom:2px solid #016E81;" })
                    .position({ bottom:85, centerX:0 })

                ___("logo")
                    .image(asset("logo.png"), { width:100, height:21, fit:true })
                    .position({ centerX:0, bottom:30 })

            sr.loading.done(callback);
        }
    }
}


function addClicktag(customUrl) {

    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {

        if(__("zipcode").value !== ""){
            zipcode_input = __("zipcode").value;
            number_input = __("number").value;
            Enabler.exitQueryString("address", "zipcode="+ zipcode_input + "&number="+ number_input);            
                } else {
            Enabler.exit('infopage');
        }

    }

}

module.exports.setElements = setElements;