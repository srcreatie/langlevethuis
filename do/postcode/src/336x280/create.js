function setElements(callback) {

    config = {};
    config.bannerWidth = 336;
    config.bannerHeight = 280;

    if (Enabler.isInitialized()) {
        enablerInitHandler();
    } else {
        Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler);
    }

    function enablerInitHandler() {

        if (Enabler.isPageLoaded()) {
            pageLoadedHandler();
        } else {

            Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED,
                pageLoadedHandler);
        }
    }

    function pageLoadedHandler() {

        addClicktag(dd.exit_url);
        loadFont();

        function loadFont() {
            sr.font.add([asset('bold.woff'),
                asset('semibold.woff')
            ], add);
        }

        function add() {

            ___("bg")
                .image(asset("bg-336x280.jpg"), { width:336, height:280, fit:true })

            ___("pink")
                .style({ background:"#D92266", css:"padding:11px" })
                .position({ left:14, top:14 })

                ___("pink>h1")
                    .text("Zorgeloos besparen", { fontSize:22, color:"#fff", webfont:"bold" })
                    .style({ css:"position:relative;" })

                ___("pink>h2")
                    .text("met de zon als bron", { fontSize:22, color:"#fff", webfont:"semibold" })
                    .style({ css:"position:relative;" })

                    ___("h3")
                        .text("Doe de zonnepanelencheck!", { fontSize:22, color:"#fff", webfont:"bold" })
                        .position({ left:14, top:14 })
                    
                    ___("h4")
                        .text("Ontdek binnen één minuut hoeveel je kunt besparen", { fontSize:22, color:"#fff", webfont:"semibold" })
                        .position({ left:14, push:{ el:__("h3"), bottom:5 } })

                    ___("h5")
                        .text("Vul je postcode in en ontvang persoonlijk advies", { fontSize:22, color:"#fff", webfont:"semibold" })
                        .position({ left:14, push:{ el:__("h3"), bottom:5 } })

            ___("zipcode")
                .style({ greensock:{ alpha:0, scale:0 } })

            ___("number")    
                .style({ greensock:{ alpha:0, scale:0 } })

            ___("zipcode_label")
                .text("Postcode", { fontSize:14, color:"#fff", webfont:"semibold" })
                .style({ greensock:{ alpha:0, scale:0 } })
                .position({ left:20, top:119 })

            ___("number_label")
                .text("Huisnr. & toev.", { fontSize:14, color:"#fff", webfont:"semibold" })
                .style({ greensock:{ alpha:0, scale:0 } })
                .position({ left:156, top:119 })

            ___("footer")
                .style({ width:336, height:53, background:"#fff" })
                .position({ bottom:0, left:0 })

                ___("footer>cta")
                    .text("Bereken besparing", { webfont:"bold", fontSize:13, color:"#fff", textAlign:"center" })
                    .style({ background:"#02B5CE", css:"border-radius:3px; padding:7px 11px; border-bottom:2px solid #016E81;" })
                    .position({ top:12, left:14 })

                ___("footer>logo")
                    .image(asset("logo.png"), { width:80, height:18, fit:true })
                    .position({ right:14, top:18 })

            sr.loading.done(callback);
        }
    }
}


function addClicktag(customUrl) {

    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {

        if(__("zipcode").value !== ""){
            zipcode_input = __("zipcode").value;
            number_input = __("number").value;
            Enabler.exitQueryString("address", "zipcode="+ zipcode_input + "&number="+ number_input);            
                } else {
            Enabler.exit('infopage');
        }

    }

}

module.exports.setElements = setElements;