function setElements(callback) {

    config = {};
    config.bannerWidth = 728;
    config.bannerHeight = 90;

    if (Enabler.isInitialized()) {
        enablerInitHandler();
    } else {
        Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler);
    }

    function enablerInitHandler() {

        if (Enabler.isPageLoaded()) {
            pageLoadedHandler();
        } else {

            Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED,
                pageLoadedHandler);
        }
    }

    function pageLoadedHandler() {

        addClicktag(dd.exit_url);
        loadFont();

        function loadFont() {
            sr.font.add([asset('bold.woff'),
                asset('semibold.woff')
            ], add);
        }

        function add() {

            ___("bg")
                .image(asset("bg-728x90.jpg"), { width:728, height:90, fit:true })

            ___("pink")
                .style({ background:"#D92266", css:"padding:11px" })
                .position({ left:10, top:10 })

                ___("pink>h1")
                    .text("Zorgeloos besparen", { fontSize:20, color:"#fff", webfont:"bold" })
                    .style({ css:"position:relative;" })

                ___("pink>h2")
                    .text("met de zon als bron", { fontSize:20, color:"#fff", webfont:"semibold" })
                    .style({ css:"position:relative;" })

                    ___("h3")
                        .text("Doe de zonnepanelencheck!", { fontSize:19, color:"#fff", webfont:"bold" })
                        .position({ left:14, top:8 })
                    
                    ___("h4")
                        .text("Ontdek binnen één minuut hoeveel u kunt besparen", { fontSize:19, width:270, color:"#fff", webfont:"semibold" })
                        .position({ left:16, push:{ el:__("h3"), bottom:0 } })

                    ___("h5")
                        .text("Vul uw postcode in en ontvang persoonlijk advies", { fontSize:19, width:270, color:"#fff", webfont:"semibold" })
                        .position({ left:16, push:{ el:__("h3"), bottom:0 } })

            ___("zipcode")
                .style({ greensock:{ alpha:0, scale:0 } })

            ___("number")    
                .style({ greensock:{ alpha:0, scale:0 } })

            ___("zipcode_label")
                .text("Postcode", { fontSize:11, color:"#fff", webfont:"semibold" })
                .style({ greensock:{ alpha:0, scale:0 } })
                .position({ left:305, top:23 })

            ___("number_label")
                .text("Huisnr. & toev.", { fontSize:11, color:"#fff", webfont:"semibold" })
                .style({ greensock:{ alpha:0, scale:0 } })
                .position({ left:430, top:23 })

            ___("footer")
                .style({ width:208, height:90, background:"#fff" })
                .position({ bottom:0, right:0 })

                ___("cta")
                    .text("Bereken besparing", { webfont:"bold", fontSize:13, color:"#fff", textAlign:"center" })
                    .style({ background:"#02B5CE", css:"border-radius:3px; padding:7px 11px; border-bottom:2px solid #016E81;" })
                    .position({ bottom:12, right:14 })

                ___("logo")
                    .image(asset("logo.png"), { width:80, height:18, fit:true })
                    .position({ right:14, top:18 })

            sr.loading.done(callback);
        }
    }
}


function addClicktag(customUrl) {

    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {

        if(__("zipcode").value !== ""){
            zipcode_input = __("zipcode").value;
            number_input = __("number").value;
            Enabler.exitQueryString("address", "zipcode="+ zipcode_input + "&number="+ number_input);            
                } else {
            Enabler.exit('infopage');
        }

    }

}

module.exports.setElements = setElements;