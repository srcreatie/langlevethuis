function animate(callback) {

    setAnimation();

    function setAnimation() {

        var tl01 = new TimelineMax();
        var tlPowerFlow = new TimelineMax();

        tlPowerFlow.to([__("flowing1"), __("flowing2")], 25, { x:800, ease:Power0.easeNone })

        tl01.to(__("creative"), 0, { alpha:1 })
        
        //copy
        tl01.from(__("pink"), 0.5, { x:-20, alpha:0 })
        tl01.from(__("h1"), 0.5, { alpha:0 }, "firstFrame")
        tl01.from(__("h2"), 0.5, { alpha:0 }, "firstFrame+=0.2")
        tl01.to(this, 1.5, {})
        tl01.to(__("h1"), 0.5, { alpha:0 }, "firstFrameOut")
        tl01.to(__("h2"), 0.5, { alpha:0 }, "firstFrameOut+=0.2")
        tl01.to(__("pink"), 0.5, { x:-14, y:-14, width:160, height:600 })
        tl01.from(__("h3"), 0.5, { alpha:0 }, "secondFrame")
        tl01.from(__("h4"), 0.5, { alpha:0 }, "secondFrame+=0.2")
        tl01.to(this, 1.5, {})
        tl01.to(__("h4"), 0.5, { alpha:0 } )
        tl01.from(__("h5"), 0.5, { alpha:0 })

        //show input
        tl01.to(__("zipcode_label"), 1, { scale:1, alpha:1, ease:Back.easeInOut.config(1.4) }, "inputPop")
        tl01.to(__("number_label"), 1, { scale:1, alpha:1, ease:Back.easeInOut.config(1.4) }, "inputPop+=0.2")
        tl01.to(__("zipcode"), 1, { scale:1, alpha:1, ease:Back.easeInOut.config(1.4) }, "inputPop+=0.4")
        tl01.to(__("number"), 1, { scale:1, alpha:1, ease:Back.easeInOut.config(1.4) }, "inputPop+=0.6")

        tl01.to(this, 1, {})

        //pulse
        tl01.to(__("cta"), 0.5, { scale:1.1, yoyo:true, repeat:1, ease:Power2.easeInOut, transformOrigin:"center center" })
        tl01.to(__("savings_sticker"), 0.5, { scale:1.1, yoyo:true, repeat:1, ease:Power2.easeInOut, transformOrigin:"center center" })

        if (srBanner.debug) {

            if (srBanner.debug && srBanner.pauseFrom) {

                console.log("pause from " + srBanner.pauseFrom);
                tl01.pause(srBanner.pauseFrom);
            }

            if (srBanner.debug && srBanner.playFrom) {

                console.log("play from " + srBanner.playFrom);
                tl01.pause(srBanner.playFrom);
            }


            if (srBanner && srBanner.backupImage) {

                console.log("create backup images of last frame");
                tl01.add("screenshot");
                tl01.pause("screenshot");
            }
        }

    }

    if (callback) {

        callback();

    }

}

module.exports.animate = animate;