function setElements(callback) {

    config = {};
    config.bannerWidth = 336;
    config.bannerHeight = 280;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('bold.woff'),
            asset('semibold.woff')
        ], add);
    }

    function add() {

        ___("bg")
            .image(asset("bg-336x280.jpg"), { width:336, height:280, fit:true })

        ___("pink")
            .style({ background:"#D92266", css:"padding:11px" })
            .position({ left:14, top:14 }) 

            ___("pink>h1")
                .text("Gefeliciteerd!", { fontSize:30, color:"#fff", webfont:"bold" })
                .style({ css:"position:relative;" })

            ___("pink>h2")
                .text("Uw dak is geschikt", { fontSize:22, color:"#fff", webfont:"semibold" })
                .style({ css:"position:relative;" })

                ___("h3")
                    .text("Zorgeloos besparen?", { fontSize:22, color:"#fff", webfont:"semibold" })
                    .position({ left:14, top:14 })
                
                if (dd.huurkoop == "huur") {
                ___("h4")
                    .text("Huur uw <span id=\"bold\">zonnepanelen</span>", { fontSize:22, color:"#fff", webfont:"semibold" })
                    .position({ left:14, push:{ el:__("h3"), bottom:1 } })
                }else{
                ___("h4")
                    .text("Investeer in <span id=\"bold\">zonnepanelen</span>", { fontSize:22, color:"#fff", webfont:"semibold" })
                    .position({ left:14, push:{ el:__("h3"), bottom:1 } })
                }

            /* lijntjes */
            // ___("solar_power1")
            //     .style({ width:75, height:5, css:"overflow:hidden", greensock:{ rotation:5 } })
            //     .position({ left:150, top:125 })

            // ___("solar_power1>flowing1")
            //     .style({ width:10000, height:2, css:"border-top:2px dashed #fff;" })
            //     .position({ right:0 })

            // ___("solar_power2")
            //     .style({ width:75, height:5, css:"overflow:hidden", greensock:{ rotation:-5 } })
            //     .position({ left:150, top:175 })

            // ___("solar_power2>flowing2")
            //     .style({ width:10000, height:2, css:"border-top:2px dashed #fff;" })
            //     .position({ right:0 })

            /* zonnepanelen */
            ___("solar1")
                .image(asset("solar.png"), { width:33, height:54, fit:true })
                .position({ left:14, top:95 })

                ___("flare")
                    .image(asset("flare.png"), {width:608, height:502})
                    .position({ left:-160, top:-100 })

            ___("solar2")
                .image(asset("solar.png"), { width:33, height:54, fit:true })
                .position({ el:__("solar1"), top:0, push:{ el:__("solar1"), right:3 } })

            ___("solar3")
                .image(asset("solar.png"), { width:33, height:54, fit:true })
                .position({ el:__("solar2"), top:0, push:{ el:__("solar2"), right:3 } })

            ___("solar4")
                .image(asset("solar.png"), { width:33, height:54, fit:true })
                .position({ el:__("solar3"), top:0, push:{ el:__("solar3"), right:3 } })

            ___("solar5")
                .image(asset("solar.png"), { width:33, height:54, fit:true })
                .position({ el:__("solar1"), left:0, push:{ el:__("solar1"), bottom:5 } })

            ___("solar6")
                .image(asset("solar.png"), { width:33, height:54, fit:true })
                .position({ el:__("solar5"), top:0, push:{ el:__("solar5"), right:3 } })

            ___("solar7")
                .image(asset("solar.png"), { width:33, height:54, fit:true })
                .position({ el:__("solar6"), top:0, push:{ el:__("solar6"), right:3 } })

            ___("solar8")
                .image(asset("solar.png"), { width:33, height:54, fit:true })
                .position({ el:__("solar7"), top:0, push:{ el:__("solar7"), right:3 } })
                
            ___("savings_sticker")
                .style({ width:100, height:100, background:"#02B5CE", css:"border-radius:100%;", greensock:{ rotation:3 } })
                .position({ right:10, top:100 })

                ___("savings_sticker>bespaar")
                    .text("Investering", { fontSize:14, color:"#fff", webfont:"semibold" })
                    .position({ left:17, top:25 })

                ___("savings_sticker>euro")
                    .text("€ 0,-", { fontSize:24, color:"#fff", webfont:"bold"  })
                    .position({ left:25, top:46 })

            if (dd.huurkoop == "huur") {
            /* alleen met huur */
             ___("savings_sticker_back")
                .style({ width:100, height:100, background:"#02B5CE", css:"border-radius:100%;", greensock:{ rotation:3 } })
                .position({ right:10, top:100 })


                ___("savings_sticker_back>bespaar_tot_back")
                    .text("Maandelijks", { fontSize:14, color:"#fff", webfont:"semibold" })
                    .position({ left:14, top:32 })

                ___("savings_sticker_back>euro_back")
                    .text("opzegbaar!", { fontSize:14, color:"#fff", webfont:"semibold"  })
                    .position({ left:15, top:48 })
            }

        ___("footer")
            .style({ width:336, height:53, background:"#fff" })
            .position({ bottom:0, left:0 })

            ___("footer>cta")
                .text("Gratis Offerte", { webfont:"bold", fontSize:15, color:"#fff", textAlign:"center" })
                .style({ background:"#ff6600", css:"border-radius:3px; padding:7px 11px;" })
                .position({ top:9, right:14 })

            ___("footer>logo")
                .image(asset("logo.png"), { width:96, height:21, fit:true })
                .position({ left:14, top:16 })

        sr.loading.done(callback);
    }
}


function addClicktag(customUrl) {

    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {

        if (srBanner.dcs) {

            Enabler.exit('Background Exit');

        } else {

            window.open(clickTag);

        }

    }

}

module.exports.setElements = setElements;