function setElements(callback) {

    config = {};
    config.bannerWidth = 728;
    config.bannerHeight = 90;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('bold.woff'),
            asset('semibold.woff')
        ], add);
    }

    function add() {

        ___("bg")
            .image(asset("bg-728x90.jpg"), { width:728, height:90, fit:true })
            .position({ left:0, top:0 })

        ___("pink")
            .style({ background:"#D92266", css:"padding:9px" })
            .position({ left:7, top:7 }) 

            ___("pink>h1")
                .text("Gefeliciteerd!", { fontSize:26, color:"#fff", webfont:"bold" })
                .style({ css:"position:relative;" })

            ___("pink>h2")
                .text("Uw dak is geschikt", { fontSize:20, color:"#fff", webfont:"semibold" })
                .style({ css:"position:relative;" })

                ___("h3")
                    .text("Zorgeloos besparen?", { fontSize:20, color:"#fff", webfont:"semibold" })
                    .position({ left:14, top:20 })
                
                if (dd.huurkoop == "huur") {
                ___("h4")
                    .text("Huur uw <span id=\"bold\">zonnepanelen</span>", { fontSize:20, color:"#fff", webfont:"semibold" })
                    .position({ left:14, push:{ el:__("h3"), bottom:1 } })
                }else{
                ___("h4")
                    .text("Investeer in <span id=\"bold\">zonnepanelen</span>", { fontSize:20, color:"#fff", webfont:"semibold" })
                    .position({ left:14, push:{ el:__("h3"), bottom:1 } })
                }

            /* lijntjes */
            // ___("solar_power1")
            //     .style({ width:75, height:5, css:"overflow:hidden", greensock:{ rotation:0 } })
            //     .position({ left:400, centerY:6 })

            // ___("solar_power1>flowing1")
            //     .style({ width:10000, height:2, css:"border-top:2px dashed #fff;" })
            //     .position({ right:0 })

            // ___("solar_power2")
            //     .style({ width:75, height:5, css:"overflow:hidden", greensock:{ rotation:-5 } })
            //     .position({ left:140, top:150 })

            // ___("solar_power2>flowing2")
            //     .style({ width:10000, height:2, css:"border-top:2px dashed #fff;" })
            //     .position({ right:0 })

            /* zonnepanelen */
            ___("solar1")
                .image(asset("solar.png"), { width:34, height:49, fit:true })
                .position({ left:284, top:25 })

                ___("flare")
                    .image(asset("flare.png"), {width:608, height:502})
                    .position({ left:110, top:-160 })

            ___("solar2")
                .image(asset("solar.png"), { width:34, height:49, fit:true })
                .position({ el:__("solar1"), top:0, push:{ el:__("solar1"), right:3 } })

            ___("solar3")
                .image(asset("solar.png"), { width:34, height:49, fit:true })
                .position({ el:__("solar2"), top:0, push:{ el:__("solar2"), right:3 } })

            ___("solar4")
                .image(asset("solar.png"), { width:34, height:49, fit:true })
                .position({ el:__("solar3"), top:0, push:{ el:__("solar3"), right:3 } })
                
            ___("savings_sticker")
                .style({ width:90, height:90, background:"#02B5CE", css:"border-radius:100%;", greensock:{ rotation:3 } })
                .position({ right:200, top:0 })

                ___("savings_sticker>bespaar")
                    .text("Investering", { fontSize:13, color:"#fff", webfont:"semibold" })
                    .position({ left:15, top:20 })

                ___("savings_sticker>euro")
                    .text("€ 0,-", { fontSize:23, color:"#fff", webfont:"bold"  })
                    .position({ left:20, top:36 })

            if (dd.huurkoop == "huur") {
            /* alleen met huur */
             ___("savings_sticker_back")
                .style({ width:90, height:90, background:"#02B5CE", css:"border-radius:100%;", greensock:{ rotation:3 } })
                .position({ right:200, top:0 })


                ___("savings_sticker_back>bespaar_tot_back")
                    .text("Maandelijks", { fontSize:13, color:"#fff", webfont:"semibold" })
                    .position({ left:12, top:28 })

                ___("savings_sticker_back>euro_back")
                    .text("opzegbaar!", { fontSize:13, color:"#fff", webfont:"semibold"  })
                    .position({ left:13, top:43 })
            }

        ___("footer")
            .style({ width:200, height:90, background:"#fff" })
            .position({ bottom:0, right:0 })

            ___("cta")
                .text("Gratis Offerte", { webfont:"bold", fontSize:13, color:"#fff", textAlign:"center" })
                .style({ background:"#ff6600", css:"border-radius:3px; padding:7px 11px;" })
                .position({ bottom:12, right:12 })

            ___("logo")
                .image(asset("logo.png"), { width:80, height:18, fit:true })
                .position({ top:12, right:12 })

        sr.loading.done(callback);
    }
}


function addClicktag(customUrl) {

    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {

        if (srBanner.dcs) {

            Enabler.exit('Background Exit');

        } else {

            window.open(clickTag);

        }

    }

}

module.exports.setElements = setElements;