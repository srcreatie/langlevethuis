function setData(callback) {

    var devDynamicContent = {};

    devDynamicContent.srFeed = [{}];
    devDynamicContent.srFeed[0]._id = 0;
    devDynamicContent.srFeed[0].isWorking = "Default feed 2";
    devDynamicContent.srFeed[0].exit_url = "";
    devDynamicContent.srFeed[0].copy = {};
    devDynamicContent.srFeed[0].copy.h1 = "Hoe handig zou het zijn om je eigen hijskraan te hebben. think about it..."
    devDynamicContent.srFeed[0].color = {};
    devDynamicContent.srFeed[0].style = {};
    devDynamicContent.srFeed[0].style.headlineColor = "#0000FF" ; 
    devDynamicContent.srFeed[0].background300x250 = dimension("example-300x250.png", "300x250");
    devDynamicContent.srFeed[0].background300x600 = dimension("example-300x600.png", "300x600");
    devDynamicContent.srFeed[0].huurkoop = "huur";
    devDynamicContent.srFeed[0].besparing = 250;

    return devDynamicContent;
}

module.exports = setData;