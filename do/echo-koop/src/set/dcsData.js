function setup(callback) {

    Enabler.setProfileId(10038177);
    var devDynamicContent = {};

    devDynamicContent.Profile= [{}];
    devDynamicContent.Profile[0]._id = 0;
    devDynamicContent.Profile[0].besparing = ["250"];
    devDynamicContent.Profile[0].huurkoop = ["koop"];
    Enabler.setDevDynamicContent(devDynamicContent);


    if (srBanner.debug) {

        console.log("%c ==============feed=============", 'background: #0060a1; color: #FFFFFF');
        console.log("USING GOOGLE DYNAMIC FEED");
        console.log("%c _______________________________", 'background: #0060a1; color: #FFFFFF');

    }

    Enabler.setDevDynamicContent(devDynamicContent);

    if (Enabler.isInitialized()) {
        enablerInitHandler();
    } else {
        Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler);
    }

    function enablerInitHandler() {

        if (Enabler.isPageLoaded()) {
            pageLoadedHandler();
        } else {

            Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED,
                pageLoadedHandler);
        }
    }

    function pageLoadedHandler() {

        var dd = dynamicContent.Profile[0];

        dcsDataSetupDone(dd)

    }

    function dcsDataSetupDone(dd) {

        callback(dd);

    }

}

module.exports.setup = setup;