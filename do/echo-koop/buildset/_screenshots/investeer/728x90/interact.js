function animate(callback) {

    setAnimation();

    function setAnimation() {

        solar_panels = [__("solar1"), __("solar2"), __("solar3"), __("solar4"), __("solar5"), __("solar6"), __("solar7"), __("solar8")]
        savings = {saving:0};

        function toSavings() {
            TweenMax.to(savings, 2, {saving:"+="+dd.besparing[0], roundProps:"saving", onUpdate:updateHandler, ease:Linear.easeNone});
        }

        function updateHandler() {
            __("besparing").innerHTML = savings.saving;
        }

        var tl01 = new TimelineMax();
        var tlRollover = new TimelineMax({ paused:true });
        var tlPowerFlow = new TimelineMax();

        tlPowerFlow.to([__("flowing1"), __("flowing2")], 25, { x:800, ease:Power0.easeNone })

        tl01.to([__("savings_sticker"), __("savings_sticker_back")], 0, { scale:0.9 })
        tl01.to(__("creative"), 0, { alpha:1 })
        
        //copy
        tl01.from(__("pink"), 0.5, { x:-20, alpha:0 })
        tl01.from(__("h1"), 0.5, { alpha:0 }, "firstFrame")
        tl01.from(__("h2"), 0.5, { alpha:0 }, "firstFrame+=0.2")
        tl01.to(this, 1.5, {})
        tl01.to(__("h1"), 0.5, { alpha:0 }, "firstFrameOut")
        tl01.to(__("h2"), 0.5, { alpha:0 }, "firstFrameOut+=0.2")
        tl01.to(__("pink"), 0.5, { x:-14, y:-14, width:728, height:90 })
        tl01.from(__("h3"), 0.5, { alpha:0 }, "secondFrame")
        tl01.from(__("h4"), 0.5, { alpha:0 }, "secondFrame+=0.2")

        //solar + power flow
        tl01.staggerFrom(solar_panels, 1, { scale:0, alpha:0, ease:Back.easeInOut.config(1.4) }, 0.2)
        tl01.from(__("flare"), 2, { alpha:0, scale:1.2 }, "sticker-=0.5")
        tl01.from(__("savings_sticker"), 0.5, { alpha:0, scale:4 }, "sticker-=0.5")
        tl01.from([__("solar_power1"), __("solar_power2")], 0.5, { alpha:0, onComplete:toSavings })


        //pulse
        tl01.to(__("cta"), 0.5, { scale:1.1, yoyo:true, repeat:1, ease:Power2.easeInOut, transformOrigin:"center center" }, "-=0.4")
        tl01.to(__("savings_sticker"), 0.5, { scale:1.1, yoyo:true, repeat:1, ease:Power2.easeInOut, transformOrigin:"center center" })

        tl01.to(this, 0, { onComplete:rollover })
        tlRollover.to(__("savings_sticker"), 0.5, { scale:1.1, yoyo:true, repeat:1, ease:Power2.easeInOut, transformOrigin:"center center" })
        tlRollover.to(__("cta"), 0.5, { scale:1.1, yoyo:true, repeat:1, ease:Power2.easeInOut, transformOrigin:"center center" }, "rollover-=0.4")

        function rollover(){
            __("banner").onmouseover = function(){
                tlRollover.play(0);
            };

            __("banner").onmouseout = function(){
                
                tlRollover.reverse();
            };
        }

        if (srBanner.debug) {

            if (srBanner.debug && srBanner.pauseFrom) {

                console.log("pause from " + srBanner.pauseFrom);
                tl01.pause(srBanner.pauseFrom);
            }

            if (srBanner.debug && srBanner.playFrom) {

                console.log("play from " + srBanner.playFrom);
                tl01.pause(srBanner.playFrom);
            }


            if (srBanner && srBanner.backupImage) {

                console.log("create backup images of last frame");
                tl01.add("screenshot");
                tl01.pause("screenshot");
            }
        }

    }

    if (callback) {

        callback();

    }

}

module.exports.animate = animate;