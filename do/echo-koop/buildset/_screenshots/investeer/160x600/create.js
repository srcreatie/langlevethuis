function setElements(callback) {

    config = {};
    config.bannerWidth = 160;
    config.bannerHeight = 600;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('bold.woff'),
            asset('semibold.woff')
        ], add);
    }

    function add() {

        ___("bg")
            .image(asset("bg-160x600.jpg"), { width:160, height:600, wrap:true })

        ___("pink")
            .style({ background:"#D92266", css:"padding:9px 7px;" })
            .position({ left:10, top:36 }) 

            ___("pink>h1")
                .text("Gefeliciteerd!", { fontSize:19, color:"#fff", webfont:"bold" })
                .style({ css:"position:relative;" })

            ___("pink>h2")
                .text("Uw dak <br />is geschikt", { fontSize:19, color:"#fff", webfont:"semibold" })
                .style({ css:"position:relative;" })

                ___("h3")
                    .text("Zorgeloos besparen?", { fontSize:20, color:"#fff", webfont:"semibold" })
                    .position({ left:14, top:20 })
                
                ___("h4")
                    .text("Investeer in <span id=\"bold\">zonnepanelen</span>", { fontSize:20, color:"#fff", webfont:"semibold" })
                    .position({ left:14, push:{ el:__("h3"), bottom:1 } })

            /* lijntjes */
            ___("solarpanels>solar_power1")
                .style({ width:75, height:5, css:"overflow:hidden", greensock:{ rotation:82 } })
                .position({ left:0, top:310 })

            ___("solarpanels>solar_power1>flowing1")
                .style({ width:10000, height:2, css:"border-top:2px dashed #fff;" })
                .position({ right:0 })

            ___("solarpanels>solar_power2")
                .style({ width:75, height:5, css:"overflow:hidden", greensock:{ rotation:100 } })
                .position({ left:44, top:310 })

            ___("solarpanels>solar_power2>flowing2")
                .style({ width:10000, height:2, css:"border-top:2px dashed #fff;" })
                .position({ right:0 })

            /* zonnepanelen */
            ___("solarpanels>solarpanels>solar1")
                .image(asset("solar.png"), { width:41, height:65, fit:true })
                .position({ left:15, top:160 })

                ___("flare")
                    .image(asset("flare.png"), {width:608, height:502})
                    .position({ left:-150, top:-30 })

            ___("solarpanels>solar2")
                .image(asset("solar.png"), { width:41, height:65, fit:true })
                .position({ el:__("solar1"), top:0, push:{ el:__("solar1"), right:6 } })

            ___("solarpanels>solar3")
                .image(asset("solar.png"), { width:41, height:65, fit:true })
                .position({ el:__("solar1"), left:0, push:{ el:__("solar1"), bottom:10 } })

            ___("solarpanels>solar4")
                .image(asset("solar.png"), { width:41, height:65, fit:true })
                .position({ el:__("solar3"), top:0, push:{ el:__("solar3"), right:6 } })

            ___("solarpanels").position({left:22})

                
            ___("savings_sticker")
                .style({ width:90, height:90, background:"#02B5CE", css:"border-radius:100%;", greensock:{ rotation:3 } })
                .position({ centerX:0, top:347 })

                ___("savings_sticker>bespaar_tot")
                    .text("Uw besparing", { fontSize:12, color:"#fff", webfont:"semibold" })
                    .position({ left:11, top:23 })

                ___("savings_sticker>euro")
                    .text("€", { fontSize:23, color:"#fff", webfont:"bold"  })
                    .position({ left:8, top:36 })

                ___("savings_sticker>besparing")
                    .text("0", { fontSize:23, color:"#fff", textAlign:"center", width:40, webfont:"bold"  })
                    .position({ left:24, top:36 })

                ___("savings_sticker>per_jaar")
                    .text("p/j", { fontSize:10, color:"#fff", webfont:"semibold"  })
                    .position({ left:68, top:47 })

        ___("footer")
            .style({ width:160, height:134, background:"#fff" })
            .position({ bottom:0, left:0 })

            ___("footer>cta")
                .text("Ontvang &euro; 250,- extra korting", { webfont:"bold", fontSize:16, color:"#fff", textAlign:"center" })
                .style({ background:"#ff6600", css:"border-radius:3px; padding:7px 10px; max-width:116px;" })
                .position({ top:50, centerX:0 })

            ___("footer>logo")
                .image(asset("logo.png"), { width:80, height:18, fit:true })
                .position({ centerX:0, top:16 })

        sr.loading.done(callback);
    }
}


function addClicktag(customUrl) {

    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {
        Enabler.exit('Background Exit');
    }

}

module.exports.setElements = setElements;