function setElements(callback) {

    config = {};
    config.bannerWidth = 970;
    config.bannerHeight = 250;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('bold.woff'),
            asset('semibold.woff')
        ], add);
    }

    function add() {

        ___("bg_1")
            .image(dd.background970x250_1, { width:970, height:250, fit:true })

        ___("bg_2")
            .image(dd.background970x250_2, { width:970, height:250, fit:true })
            .style({ greensock:{ alpha:0 } })

        ___("h1")
            .text(dd.copy.h1, { fontSize:34, color:"rgba(255,255,255,0)", webfont:"semibold" })
            .style({ background:"#D92266", css:"padding:11px; max-width:200px;" })
            .position({ left:24, top:24 })

        ___("h2")
            .text(dd.copy.h2, { fontSize:34, color:"rgba(255,255,255,0)", webfont:"semibold" })
            .style({ background:"#D92266", css:"padding:11px; max-width:200px;" })
            .position({ left:24, top:24 })

        ___("pink")
            .style({ width:config.bannerWidth, height:config.bannerHeight, background:"#D92266" })

        ___("h3")
            .text(dd.copy.h3, { fontSize:32, color:"rgba(255,255,255,1)", webfont:"bold" })
            .style({ css:"max-width:265px;" })
            .position({ left:24, centerY:0 })

        if (dd.usp) {
            ___("usp1")
                .text(dd.copy.usp1, { fontSize:16, color:"#fff", webfont:"semibold" })
                .style({ greensock: { opacity:0 }, css:"max-width:250px;" })
                .position({ top:70, push:{ el:__("h3"), right:50 } })

            ___("usp2")
                .text(dd.copy.usp2, { fontSize:16, color:"#fff", webfont:"semibold" })
                .style({ greensock: { opacity:0 }, css:"max-width:250px;" })
                .position({ el:__("usp1"), left:0, push:{ el:__("usp1"), bottom:15 } })

            ___("usp3")
                .text(dd.copy.usp3, { fontSize:16, color:"#fff", webfont:"semibold" })
                .style({ greensock: { opacity:0 }, css:"max-width:250px;" })
                .position({ el:__("usp2"), left:0, push:{ el:__("usp2"), bottom:15 } })

            ___("checkmark1")
                .image(asset('checkmark.png'), { width:20, height:20, fit:true })
                .style({ greensock: { opacity:0 } })
                .position({ el:__("usp1"), top:0, push:{ el:__("usp1"), left:5 } })

            ___("checkmark2")
                .image(asset('checkmark.png'), { width:20, height:20, fit:true })
                .style({ greensock: { opacity:0 } })
                .position({ el:__("usp2"), top:0, push:{ el:__("usp2"), left:5 } })

            ___("checkmark3")
                .image(asset('checkmark.png'), { width:20, height:20, fit:true })
                .style({ greensock: { opacity:0 } })
                .position({ el:__("usp3"), top:0, push:{ el:__("usp3"), left:5 } })
        }else{
            ___("h4")
                .text(dd.copy.h4, { fontSize:12, color:"rgba(255,255,255,1)", webfont:"semibold" })
                .style({ css:"max-width:125px;", greensock: { opacity:0 } })
                .position({ left:14, push:{ el:__("h3"), bottom:5  }})
        }

        ___("product_image")
            .image(dd.product_image, { width:200, height:190, fit:true })
            .position({ left:585, top:30 })

        if(dd.product_image_2){
            ___("product_image_2")
                .image(dd.product_image_2, { width:200, height:190, fit:true })
                .position({ left:585, top:30 })
        }

        ___("footer")
            .style({ width:176, height:250, background:"#fff" })
            .position({ bottom:0, right:0 })

            ___("cta")
                .text(dd.cta, { webfont:"bold", fontSize:16, color:"#fff", textAlign:"center" })
                .style({ background:"#02B5CE", css:"max-width:140px; border-radius:3px; padding:7px 11px; border-bottom:2px solid #016E81;" })
                .position({ bottom:15, el: __("footer"), centerX:0 })

            ___("logo")
                .image(asset("logo.png"), { width:120, height:26, fit:true })
                .position({ right:25, top:40 })

        sr.loading.done(callback);
    }
}


function addClicktag(customUrl) {

    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {

        if (srBanner.dcs) {

            Enabler.exit('Background Exit');

        } else {

            window.open(clickTag);

        }

    }

}

module.exports.setElements = setElements;