function animate(callback) {

    setAnimation();

    function setAnimation() {

        __("creative").style.opacity = 1;

        var tl01 = new TimelineMax();
        var tlRollover = new TimelineMax({ paused:true });

        tl01.from(__("h1"), 1, { x:-100, transformOrigin:"center left", scaleX:0, ease:Back.easeOut })
        tl01.to(__("h1"), 1, { color:"rgba(255,255,255,1)" })

        tl01.to(this, 1.5, {})

        tl01.to(__("h1"), 0.3, { color:"rgba(255,255,255,0)" })
        tl01.to(__("h1"), 0.6, { scaleX:0, transformOrigin:"center left" })

        tl01.from(__("h2"), 1, { x:-100, transformOrigin:"center left", scaleX:0, ease:Back.easeOut })
        tl01.to(__("h2"), 1, { color:"rgba(255,255,255,1)" })

        tl01.to(__("bg_2"), 1, { alpha:1 })
        tl01.to(this, 0.5, {})

        tl01.to(__("h2"), 0.3, { color:"rgba(255,255,255,0)" })
        tl01.to(__("h2"), 0.6, { scaleX:0, transformOrigin:"center left" })

        tl01.to(this, 0.5, {})

        tl01.from(__("pink"), 1, { scale:0, transformOrigin:"top left" })

        tl01.from(__("h3"), 0.5, { alpha:0 }, "lastFrame")
        if (dd.usp) {
            tl01.staggerTo([__("checkmark1"), __("checkmark2"), __("checkmark3")], 0.5, { alpha:1 }, 0.2, "lastFrame+=0.5")
            tl01.staggerTo([__("usp1"), __("usp2"), __("usp3")], 0.5, { alpha:1 }, 0.2, "lastFrame+=0.6")
        }else{
            tl01.to(__("h4"), 0.5, { alpha:1 }, "lastFrame+=0.5")
        }
        tl01.from(__("product_image"), 1, { y:250, ease:Back.easeOut }, "lastFrame")
        
        if(dd.product_image_2){
            tl01.from(__("product_image_2"), 1, { alpha:0 }, "lastFrame+=1.5")
            tlRollover.to(__("product_image_2"), 0.5, { alpha:0 }, "rollover")
        }

        tl01.to(__("cta"), 0.5, { scale:1.1, yoyo:true, repeat:1, ease:Power2.easeInOut, transformOrigin:"center center" })
        tl01.to(this, 0, { onComplete:rollover })

        tlRollover.to(__("cta"), 0.5, { scale:1.1, yoyo:true, repeat:1, ease:Power2.easeInOut, transformOrigin:"center center" }, "rollover")

        function rollover(){
            __("banner").onmouseover = function(){
                tlRollover.play(0);
            };

            __("banner").onmouseout = function(){
                
                tlRollover.reverse();
            };
        }

        if (srBanner.debug) {

            if (srBanner.debug && srBanner.pauseFrom) {
                console.log("pause from " + srBanner.pauseFrom);
                tl01.pause(srBanner.pauseFrom);
            }

            if (srBanner.debug && srBanner.playFrom) {
                console.log("play from " + srBanner.playFrom);
                tl01.pause(srBanner.playFrom);
            }


            if (srBanner && srBanner.backupImage) {
                console.log("create backup images of last frame");
                tl01.add("screenshot");
                tl01.pause("screenshot");
            }
        }

        // //----------------------------------------------------
        // //----------------------------------------------------
        // //----------------------------------------------------

    }

    if (callback) {

        callback();

    }

}

module.exports.animate = animate;