function setData(callback) {

    var devDynamicContent = {};

    devDynamicContent.srFeed = [{}];
    devDynamicContent.srFeed[0]._id = 0;
    devDynamicContent.srFeed[0].isWorking = "Default feed 2";
    devDynamicContent.srFeed[0].exit_url = "";
    devDynamicContent.srFeed[0].copy = {};
    devDynamicContent.srFeed[0].copy.h1 = "De lichten dimmen..."
    devDynamicContent.srFeed[0].copy.h2 = "...zonder op te staan?"
    devDynamicContent.srFeed[0].copy.h3 = "Nu gratis slim verlichtingspakket van €179,-"
    devDynamicContent.srFeed[0].copy.h3_break = "Nu gratis slim verlichtings-<br />pakket van €179,-"
    devDynamicContent.srFeed[0].copy.h4 = "Bij een 3-jarig energiecontract"
    devDynamicContent.srFeed[0].usp = false;
    devDynamicContent.srFeed[0].cta = "Pak deze deal"
    devDynamicContent.srFeed[0].versie = "verlichting"
    devDynamicContent.srFeed[0].background300x250_1 = dimension("bg-300x250-light.jpg", "300x250");
    devDynamicContent.srFeed[0].background300x250_2 = dimension("bg-300x250-dimmed.jpg", "300x250");
    devDynamicContent.srFeed[0].background320x240_1 = dimension("bg-300x250-light.jpg", "320x240");
    devDynamicContent.srFeed[0].background320x240_2 = dimension("bg-300x250-dimmed.jpg", "320x240");
    devDynamicContent.srFeed[0].background336x280_1 = dimension("bg-336x280-light.jpg", "336x280");
    devDynamicContent.srFeed[0].background336x280_2 = dimension("bg-336x280-dimmed.jpg", "336x280");
    devDynamicContent.srFeed[0].background728x90_1 = dimension("bg-728x90-light.jpg", "728x90");
    devDynamicContent.srFeed[0].background728x90_2 = dimension("bg-728x90-dimmed.jpg", "728x90");
    devDynamicContent.srFeed[0].background160x600_1 = dimension("bg-160x600-light.jpg", "160x600");
    devDynamicContent.srFeed[0].background160x600_2 = dimension("bg-160x600-dimmed.jpg", "160x600");
    devDynamicContent.srFeed[0].product_image = asset("slim.png");
    devDynamicContent.srFeed[0].product_image_2 = asset("slim2.png");

    return devDynamicContent;
}

module.exports = setData;