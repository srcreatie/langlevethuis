function setData(callback) {

    var devDynamicContent = {};

    devDynamicContent.srFeed = [{}];
    devDynamicContent.srFeed[0]._id = 0;
    devDynamicContent.srFeed[0].isWorking = "Default feed 2";
    devDynamicContent.srFeed[0].exit_url = "";
    devDynamicContent.srFeed[0].copy = {};
    devDynamicContent.srFeed[0].copy.h1 = "Op afstand je verwarming bedienen?"
    devDynamicContent.srFeed[0].copy.h2 = "Dat doe je met de <strong>slimme thermostaat</strong>"
    devDynamicContent.srFeed[0].copy.h3 = "Nu gratis Honeywell Lyric<br />van €199,-"
    devDynamicContent.srFeed[0].copy.h3_break = "Nu gratis Honeywell Lyric<br />van €199,-"
    devDynamicContent.srFeed[0].copy.h4 = "Bij een 3-jarig energiecontract"
    devDynamicContent.srFeed[0].usp = false;
    devDynamicContent.srFeed[0].cta = "Direct afsluiten"
    devDynamicContent.srFeed[0].versie = "honeywell"
    devDynamicContent.srFeed[0].background300x250_1 = dimension("bg-300x250-honey-1.jpg", "300x250");
    devDynamicContent.srFeed[0].background300x250_2 = dimension("bg-300x250-honey-2.jpg", "300x250");
    devDynamicContent.srFeed[0].background320x240_1 = dimension("bg-300x250-honey-1.jpg", "320x240");
    devDynamicContent.srFeed[0].background320x240_2 = dimension("bg-300x250-honey-2.jpg", "320x240");
    devDynamicContent.srFeed[0].background336x280_1 = dimension("bg-336x280-honey-1.jpg", "336x280");
    devDynamicContent.srFeed[0].background336x280_2 = dimension("bg-336x280-honey-2.jpg", "336x280");
    devDynamicContent.srFeed[0].background728x90_1 = dimension("bg-728x90-honey-1.jpg", "728x90");
    devDynamicContent.srFeed[0].background728x90_2 = dimension("bg-728x90-honey-2.jpg", "728x90");
    devDynamicContent.srFeed[0].background160x600_1 = dimension("bg-160x600-honey-1.jpg", "160x600");
    devDynamicContent.srFeed[0].background160x600_2 = dimension("bg-160x600-honey-2.jpg", "160x600");
    devDynamicContent.srFeed[0].product_image = asset("honey.png");
    devDynamicContent.srFeed[0].product_image_2 = false;

    return devDynamicContent;
}

module.exports = setData;