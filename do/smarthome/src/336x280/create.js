function setElements(callback) {

    config = {};
    config.bannerWidth = 336;
    config.bannerHeight = 280;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('bold.woff'),
            asset('semibold.woff')
        ], add);
    }

    function add() {

        ___("bg_1")
            .image(dd.background336x280_1, { width:336, height:280, fit:true })

        ___("bg_2")
            .image(dd.background336x280_2, { width:336, height:280, fit:true })
            .style({ greensock:{ alpha:0 } })

        ___("h1")
            .text(dd.copy.h1, { fontSize:21, color:"rgba(255,255,255,0)", webfont:"semibold" })
            .style({ background:"#D92266", css:"padding:11px; max-width:230px;" })
            .position({ left:14, top:14 })

        ___("h2")
            .text(dd.copy.h2, { fontSize:21, color:"rgba(255,255,255,0)", webfont:"semibold" })
            .style({ background:"#D92266", css:"padding:11px; max-width:230px;" })
            .position({ left:14, top:14 })

        ___("pink")
            .style({ width:config.bannerWidth, height:config.bannerHeight, background:"#D92266" })

        ___("h3")
            .text(dd.copy.h3, { fontSize:21, color:"rgba(255,255,255,1)", webfont:"bold" })
            .style({ css:"max-width:308px;" })
            .position({ left:14, top:14 })

        if (dd.usp) {
            ___("usp1")
                .text(dd.copy.usp1, { fontSize:14, color:"#fff", webfont:"semibold" })
                .style({ greensock: { opacity:0 }, css:"max-width:132px;" })
                .position({ left:40, push:{ el:__("h3"), bottom:20 } })

            ___("usp2")
                .text(dd.copy.usp2, { fontSize:14, color:"#fff", webfont:"semibold" })
                .style({ greensock: { opacity:0 }, css:"max-width:132px;" })
                .position({ left:40, push:{ el:__("usp1"), bottom:8 } })

            ___("usp3")
                .text(dd.copy.usp3, { fontSize:14, color:"#fff", webfont:"semibold" })
                .style({ greensock: { opacity:0 }, css:"max-width:132px;" })
                .position({ left:40, push:{ el:__("usp2"), bottom:8 } })

            ___("checkmark1")
                .image(asset('checkmark.png'), { width:15, height:15, fit:true })
                .style({ greensock: { opacity:0 } })
                .position({ el:__("usp1"), top:0, push:{ el:__("usp1"), left:5 } })

            ___("checkmark2")
                .image(asset('checkmark.png'), { width:15, height:15, fit:true })
                .style({ greensock: { opacity:0 } })
                .position({ el:__("usp2"), top:0, push:{ el:__("usp2"), left:5 } })

            ___("checkmark3")
                .image(asset('checkmark.png'), { width:15, height:15, fit:true })
                .style({ greensock: { opacity:0 } })
                .position({ el:__("usp3"), top:0, push:{ el:__("usp3"), left:5 } })
        }else{
            ___("h4")
                .text(dd.copy.h4, { fontSize:12, color:"rgba(255,255,255,1)", webfont:"semibold" })
                .style({ css:"max-width:125px;", greensock: { opacity:0 } })
                .position({ left:14, push:{ el:__("h3"), bottom:5  }})
        }

        ___("product_image")
            .image(dd.product_image, { width:150, height:130, fit:true })
            .position({ left:190, top:77 })

        if(dd.product_image_2){
            ___("product_image_2")
                .image(dd.product_image_2, { width:150, height:130, fit:true })
                .position({ left:190, top:77 })
        }

        ___("footer")
            .style({ width:336, height:53, background:"#fff" })
            .position({ bottom:0, left:0 })

            ___("footer>cta")
                .text(dd.cta, { webfont:"bold", fontSize:13, color:"#fff", textAlign:"center" })
                .style({ background:"#02B5CE", css:"border-radius:3px; padding:7px 11px; border-bottom:2px solid #016E81;" })
                .position({ top:12, left:14 })

            ___("footer>logo")
                .image(asset("logo.png"), { width:80, height:18, fit:true })
                .position({ right:14, top:18 })

        sr.loading.done(callback);
    }
}


function addClicktag(customUrl) {

    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {

        if (srBanner.dcs) {

            Enabler.exit('Background Exit');

        } else {

            window.open(clickTag);

        }

    }

}

module.exports.setElements = setElements;