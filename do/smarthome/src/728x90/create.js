function setElements(callback) {

    config = {};
    config.bannerWidth = 728;
    config.bannerHeight = 90;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('bold.woff'),
            asset('semibold.woff')
        ], add);
    }

    function add() {

        ___("bg_1")
            .image(dd.background728x90_1, { width:728, height:90, fit:true })

        ___("bg_2")
            .image(dd.background728x90_2, { width:728, height:90, fit:true })
            .style({ greensock:{ alpha:0 } })

        ___("h1")
            .text(dd.copy.h1, { fontSize:20, color:"rgba(255,255,255,0)", webfont:"semibold" })
            .style({ background:"#D92266", css:"padding:11px; max-width:200px;" })
            .position({ left:14, centerY:0 })

        ___("h2")
            .text(dd.copy.h2, { fontSize:20, color:"rgba(255,255,255,0)", webfont:"semibold" })
            .style({ background:"#D92266", css:"padding:11px; max-width:200px;" })
            .position({ left:14, centerY:0 })

        ___("pink")
            .style({ width:config.bannerWidth, height:config.bannerHeight, background:"#D92266" })

        ___("h3")
            .text(dd.copy.h3, { fontSize:20, color:"rgba(255,255,255,1)", webfont:"bold" })
            .style({ css:"max-width:300px;" })
            .position({ left:14, top:10 })

        if (dd.usp) {
            ___("usp1")
                .text(dd.copy.usp1, { fontSize:13, color:"#fff", webfont:"semibold" })
                .style({ greensock: { opacity:0 } })
                .position({ left:32, push:{ el:__("h3"), bottom:7 } })

            ___("usp2")
                .text(dd.copy.usp2, { fontSize:13, color:"#fff", webfont:"semibold" })
                .style({ greensock: { opacity:0 } })
                .position({ el:__("usp1"), bottom:0, push:{ el:__("usp1"), right:25 } })

            ___("checkmark1")
                .image(asset('checkmark.png'), { width:12, height:12, fit:true })
                .style({ greensock: { opacity:0 } })
                .position({ el:__("usp1"), bottom:2, push:{ el:__("usp1"), left:2 } })

            ___("checkmark2")
                .image(asset('checkmark.png'), { width:12, height:12, fit:true })
                .style({ greensock: { opacity:0 } })
                .position({ el:__("usp2"), bottom:2, push:{ el:__("usp2"), left:2 } })
        }else{
            ___("h4")
                .text(dd.copy.h4, { fontSize:12, color:"rgba(255,255,255,1)", webfont:"semibold" })
                .style({ css:"max-width:300px;", greensock: { opacity:0 } })
                .position({ left:14, push:{ el:__("h3"), bottom:2  }})
        }

        ___("product_image")
            .image(dd.product_image, { width:100, height:90, fit:true })
            .position({ left:415, top:0 })

        if(dd.product_image_2){
            ___("product_image_2")
                .image(dd.product_image_2, { width:100, height:90, fit:true })
                .position({ left:415, top:0 })
        }

        ___("footer")
            .style({ width:208, height:90, background:"#fff" })
            .position({ bottom:0, right:0 })

            ___("cta")
                .text(dd.cta, { webfont:"bold", fontSize:13, color:"#fff", textAlign:"center" })
                .style({ background:"#02B5CE", css:"border-radius:3px; padding:7px 11px; border-bottom:2px solid #016E81;" })
                .position({ bottom:12, right:14 })

            ___("logo")
                .image(asset("logo.png"), { width:80, height:18, fit:true })
                .position({ right:14, top:18 })

        sr.loading.done(callback);
    }
}


function addClicktag(customUrl) {

    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {

        if (srBanner.dcs) {

            Enabler.exit('Background Exit');

        } else {

            window.open(clickTag);

        }

    }

}

module.exports.setElements = setElements;