function setElements(callback) {

    config = {};
    config.bannerWidth = 728;
    config.bannerHeight = 90;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('bold.woff'),
            asset('semibold.woff')
        ], add);
    }

    function add() {
        ___("bg_1")
            .image(dd.background728x90_1, { width:728, height:90, fit:true });

        // ___("bg_2")
        //     .image(dd.background728x90_2, { width:728, height:90, fit:true })
        //     .style({ greensock:{ alpha:0 } });

        ___("h1")
            .text(dd.copy.h1, { fontSize:20, color:"rgba(255,255,255,0)", webfont:"semibold" })
            .style({ background:"#D92266", css:"padding:11px; max-width:210px;" })
            .position({ left:14, top:14 });

        // ___("h2")
        //     .text(dd.copy.h2, { fontSize:20, color:"rgba(255,255,255,0)", webfont:"bold" })
        //     .style({ background:"#D92266", css:"padding:11px; max-width:200px;" })
        //     .position({ left:14, top:14 });

        ___("uspBackground")
            .style({ width:config.bannerWidth, height:config.bannerHeight, background:"#D92266" })
            .position({ left:config.bannerWidth, top:0 });
            
            // Koop usps
            ___("koopVoordelen")
                .text(dd.copy.koop_end_txt, { maxFs:18, width:170, color:"#FFFFFF", webfont:"bold" })
                .style({ css:"line-height:1.2;" })
                .position({ left:20, top:10 });

            ___("check1koop")
                .image(asset("checkmark.png"), {width:15, height:15, fit:true})
                .position({el:__("koopVoordelen"), top:0, push:{el:__("koopVoordelen"), right:20}});

            ___("koopUsp1")
                .text(dd.copy.usp1_koop, { color:"#FFFFFF", webfont:"semibold", fontSize:14 })                    
                .position({el:__("check1koop"), top:0, push:{ el:__("check1koop"), right:10 }});

            ___("check2koop")
                .image(asset("checkmark.png"), {width:15, height:15, fit:true})
                .position({el:__("check1koop"), left:0, push:{el:__("check1koop"), bottom:10}});

            ___("koopUsp2")
                .text(dd.copy.usp2_koop, { color:"#FFFFFF", webfont:"semibold", fontSize:14 })                    
                .position({el:__("check2koop"), top:0, push:{ el:__("check1koop"), right:10 }});

            ___("check3koop")
                .image(asset("checkmark.png"), {width:15, height:15, fit:true})
                .position({el:__("check1koop"), left:0, push:{el:__("check2koop"), bottom:10}});

            ___("koopUsp3")
                .text(dd.copy.usp3_koop, { color:"#FFFFFF", webfont:"semibold", fontSize:14 })
                .position({el:__("check3koop"), top:0, push:{ el:__("check1koop"), right:10 }});

            // HUUR usps
            ___("huurVoordelen")
                .text(dd.copy.huur_end_txt, { maxFs:18, width:170, color:"#FFFFFF", webfont:"bold" })
                .style({ css:"line-height:1.2;" })
                .position({ left:20, top:10 });

            ___("check1huur")
                .image(asset("checkmark.png"), {width:15, height:15, fit:true})
                .position({el:__("huurVoordelen"), top:0, push:{el:__("huurVoordelen"), right:20}});

            ___("huurUsp1")
                .text(dd.copy.usp1_huur, { color:"#FFFFFF", webfont:"semibold", fontSize:14 })                    
                .position({el:__("check1huur"), top:0, push:{ el:__("check1huur"), right:10 }});

            ___("check2huur")
                .image(asset("checkmark.png"), {width:15, height:15, fit:true})
                .position({el:__("check1huur"), left:0, push:{el:__("check1huur"), bottom:10}});

            ___("huurUsp2")
                .text(dd.copy.usp2_huur, { color:"#FFFFFF", webfont:"semibold", fontSize:14 })                    
                .position({el:__("check2huur"), top:0, push:{ el:__("check1huur"), right:10 }});

            ___("check3huur")
                .image(asset("checkmark.png"), {width:15, height:15, fit:true})
                .position({el:__("check1huur"), left:0, push:{el:__("check2huur"), bottom:10}});

            ___("huurUsp3")
                .text(dd.copy.usp3_huur, { color:"#FFFFFF", webfont:"semibold", fontSize:14 })                    
                .position({el:__("check3huur"), top:0, push:{ el:__("check1huur"), right:10 }});
        
        ___("footer")
            .style({ width:208, height:90, background:"#fff" })
            .position({ bottom:0, right:0 });

        ___("cta")
            .text(dd.cta, { webfont:"bold", fontSize:13, color:"#fff", textAlign:"center" })
            .style({ background:"#00a9d1", css:"border-radius:3px; padding:7px 11px;" })
            .position({ bottom:10, right:14 });

        ___("footer>logo")
            .image(asset("logo.png"), { width:80, height:18, fit:true })
            .position({ left:114, top:16 });

        ___("banner>switch")
            .style({background:"#F493C0", width:40, height:22, addClass:"on", css:"border-radius: 100px; cursor: pointer;"})
            .position({left: 470, top: 25});

        ___("switch>round")
            .style({width:22, height:22, background:"#FFFFFF", css:"border-radius: 100%;"});                

        ___("switch_huur_txt")
            .text("Liever huren?", {webfont:"semibold", fontSize:11, color:"#fff", textAlign:"center" })
            .position({el:__("switch"), right:0, push:{el:__("switch"), top:5}});

        ___("switch_koop_txt")
            .text("Liever kopen?", {webfont:"semibold", fontSize:11, color:"#fff", textAlign:"center" })
            .position({el:__("switch"), right:0, push:{el:__("switch"), top:5}});

        ___("swipeicon")
            .image(asset("swipeicon.png"), { width:30, height:30, fit:true})
            .position({el:__("switch"), centerX:0, push:{el:__("switch"), bottom:10}});

        sr.loading.done(callback);
    }
}


function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else if(clickout == "koop") {
            window.open(clickTag1);
        } else if (clickout == "huur"){
            window.open(clickTag2);
        } else {
            window.open(clickTag);
        }
    }
}

module.exports.setElements = setElements;