function setElements(callback) {

    config = {};
    config.bannerWidth = 336;
    config.bannerHeight = 280;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('bold.woff'),
            asset('semibold.woff')
        ], add);
    }

    function add() {
        ___("bg_1")
            .image(dd.background336x280_1, { width:336, height:280, fit:true });

        // ___("bg_2")
        //     .image(dd.background336x280_2, { width:336, height:280, fit:true })
        //     .style({ greensock:{ alpha:0 } });

        ___("h1")
            .text(dd.copy.h1, { fontSize:23, color:"rgba(255,255,255,0)", webfont:"semibold" })
            .style({ background:"#D92266", css:"padding:11px; max-width:240px;" })
            .position({ left:14, top:14 });

        // ___("h2")
        //     .text(dd.copy.h2, { fontSize:23, color:"rgba(255,255,255,0)", webfont:"bold" })
        //     .style({ background:"#D92266", css:"padding:11px; max-width:220px;" })
        //     .position({ left:14, top:14 });

        ___("uspBackground")
            .style({ width:config.bannerWidth, height:config.bannerHeight, background:"#D92266" })
            .position({ left:config.bannerWidth, top:0 });
            
            // Koop usps
            ___("koopVoordelen")
                .text(dd.copy.koop_end_txt, { maxFs:20, width:190, color:"#FFFFFF", webfont:"bold" })
                .style({ css:"line-height:1.2;" })
                .position({ left:20, top:20 });

            ___("check1koop")
                .image(asset("checkmark.png"), {width:17, height:17, fit:true})
                .position({el:__("koopVoordelen"), left:0, push:{el:__("koopVoordelen"), bottom:30}});

            ___("koopUsp1")
                .text(dd.copy.usp1_koop, { color:"#FFFFFF", webfont:"semibold", fontSize:16 })                    
                .position({el:__("check1koop"), top:0, push:{ el:__("check1koop"), right:10 }});

            ___("check2koop")
                .image(asset("checkmark.png"), {width:17, height:17, fit:true})
                .position({el:__("koopVoordelen"), left:0, push:{el:__("check1koop"), bottom:17}});

            ___("koopUsp2")
                .text(dd.copy.usp2_koop, { color:"#FFFFFF", webfont:"semibold", fontSize:16 })                    
                .position({el:__("check2koop"), top:0, push:{ el:__("check1koop"), right:10 }});

            ___("check3koop")
                .image(asset("checkmark.png"), {width:17, height:17, fit:true})
                .position({el:__("koopVoordelen"), left:0, push:{el:__("check2koop"), bottom:17}});

            ___("koopUsp3")
                .text(dd.copy.usp3_koop, { color:"#FFFFFF", webfont:"semibold", fontSize:16 })
                .position({el:__("check3koop"), top:0, push:{ el:__("check1koop"), right:10 }});

            // HUUR usps
            ___("huurVoordelen")
                .text(dd.copy.huur_end_txt, { maxFs:18, width:170, color:"#FFFFFF", webfont:"bold" })
                .style({ css:"line-height:1.2;" })
                .position({ left:20, top:20 });

            ___("check1huur")
                .image(asset("checkmark.png"), {width:17, height:17, fit:true})
                .position({el:__("huurVoordelen"), left:0, push:{el:__("huurVoordelen"), bottom:30}});

            ___("huurUsp1")
                .text(dd.copy.usp1_huur, { color:"#FFFFFF", webfont:"semibold", fontSize:16 })                    
                .position({el:__("check1huur"), top:0, push:{ el:__("check1huur"), right:10 }});

            ___("check2huur")
                .image(asset("checkmark.png"), {width:17, height:17, fit:true})
                .position({el:__("huurVoordelen"), left:0, push:{el:__("check1huur"), bottom:17}});

            ___("huurUsp2")
                .text(dd.copy.usp2_huur, { color:"#FFFFFF", webfont:"semibold", fontSize:16 })                    
                .position({el:__("check2huur"), top:0, push:{ el:__("check1huur"), right:10 }});

            ___("check3huur")
                .image(asset("checkmark.png"), {width:17, height:17, fit:true})
                .position({el:__("huurVoordelen"), left:0, push:{el:__("check2huur"), bottom:17}});

            ___("huurUsp3")
                .text(dd.copy.usp3_huur, { color:"#FFFFFF", webfont:"semibold", fontSize:16 })                    
                .position({el:__("check3huur"), top:0, push:{ el:__("check1huur"), right:10 }});
        
        ___("footer")
            .style({ width:336, height:53, background:"#fff" })
            .position({ bottom:0, left:0 });

        ___("footer>cta")
            .text(dd.cta, { webfont:"bold", fontSize:14, color:"#fff", textAlign:"center" })
            .style({ background:"#00a9d1", css:"border-radius:3px; padding:7px 11px;" })
            .position({ top:12, right:14 });

        ___("footer>logo")
            .image(asset("logo.png"), { width:80, height:18, fit:true })
            .position({ left:14, top:18 });

        ___("banner>switch")
            .style({background:"#F493C0", width:46, height:26, addClass:"on", css:"border-radius: 100px; cursor: pointer;"})
            .position({left: 277, top: 35});

        ___("switch>round")
            .style({width:26, height:26, background:"#FFFFFF", css:"border-radius: 100%;"});                

        ___("switch_huur_txt")
            .text("Liever huren?", {webfont:"semibold", fontSize:13, color:"#fff", textAlign:"center" })
            .position({el:__("switch"), right:0, push:{el:__("switch"), top:5}});

        ___("switch_koop_txt")
            .text("Liever kopen?", {webfont:"semibold", fontSize:13, color:"#fff", textAlign:"center" })
            .position({el:__("switch"), right:0, push:{el:__("switch"), top:5}});

        ___("swipeicon")
            .image(asset("swipeicon.png"), { width:34, height:34, fit:true})
            .position({el:__("switch"), centerX:0, push:{el:__("switch"), bottom:10}});

        sr.loading.done(callback);
    }
}


function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else if(clickout == "koop") {
            window.open(clickTag1);
        } else if (clickout == "huur"){
            window.open(clickTag2);
        } else {
            window.open(clickTag);
        }
    }
}

module.exports.setElements = setElements;