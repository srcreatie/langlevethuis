function animate(callback) {

    setAnimation();

    function setAnimation() {
        clickout = '';
        placedPixel = false;

        __("creative").style.opacity = 1;

        var tl01 = new TimelineMax();
        var tlUspKoop = new TimelineMax({ paused:true });
        var tlUspHuur = new TimelineMax({ paused:true });
        var tlRollover = new TimelineMax({ paused:true });

        is_huur = '';
            if(dd.versie == "koop"){
                is_huur = false;
                clickout = "koop";
            } else {
                is_huur = true;
                clickout = "huur";
            }

        uspsToAnimate = [];
        uspsToAnimateOut = [];
        switchtxt = '';
        switchtxtOut = '';

        function toggleUspItems(){
            if(is_huur) {
                uspsToAnimate = [__("huurVoordelen"), __("check1huur"), __("huurUsp1"), __("check2huur"), __("huurUsp2"), __("check3huur"), __("huurUsp3")];
                uspsToAnimateOut = [__("koopVoordelen"), __("check1koop"), __("koopUsp1"), __("check2koop"), __("koopUsp2"), __("check3koop"), __("koopUsp3")];
                switchtxt = __("switch_huur_txt");
                switchtxtOut = __("switch_koop_txt");
                is_huur = true;
            } else {
                uspsToAnimate = [__("koopVoordelen"), __("check1koop"), __("koopUsp1"), __("check2koop"), __("koopUsp2"), __("check3koop"), __("koopUsp3")];
                uspsToAnimateOut = [__("huurVoordelen"), __("check1huur"), __("huurUsp1"), __("check2huur"), __("huurUsp2"), __("check3huur"), __("huurUsp3")];
                switchtxt = __("switch_koop_txt");
                switchtxtOut = __("switch_huur_txt");
                is_huur = false;
            }            
        }

        toggleUspItems();
        TweenMax.to([uspsToAnimateOut, uspsToAnimate], 0, {x:-config.bannerWidth});
        TweenMax.to([switchtxt, switchtxtOut], 0, {alpha:0, x:10});
        TweenMax.to(__("switch"), 0, {alpha:0});

        tl01.from(__("h1"), 1, { x:-100, transformOrigin:"center left", scaleX:0, ease:Back.easeOut });
        tl01.to(__("h1"), 1, { color:"rgba(255,255,255,1)" });

            tl01.to(this, 1.3, {});

        tl01.to(__("h1"), 0.3, { color:"rgba(255,255,255,0)" });
        tl01.to(__("h1"), 0.6, { scaleX:0, transformOrigin:"center left" });

        // // tl01.to(__("bg_2"), 1, { alpha:1 });

        // tl01.from(__("h2"), 1, { x:-100, transformOrigin:"center left", scaleX:0, ease:Back.easeOut });
        // tl01.to(__("h2"), 1, { color:"rgba(255,255,255,1)" });
        
        //     tl01.to(this, 1.3, {});

        // tl01.to(__("h2"), 0.3, { color:"rgba(255,255,255,0)" });
        // tl01.to(__("h2"), 0.6, { scaleX:0, transformOrigin:"center left" });

            tl01.to(this, 0.2, {});
        
        tl01.to(__("uspBackground"), 0.4, { x:-config.bannerWidth, ease:Power3.easeOut }, "end");
        tl01.staggerTo(uspsToAnimate, 0.8, {x:0, ease:Power3.easeInOut}, 0.05, "end+=0.2");
        tl01.to(switchtxtOut, 0.4, {alpha:1, x:0}, "end+=0.4");
        tl01.to(__("switch"), 0.4, {alpha:1}, "end+=0.4");

        tl01.from(__("swipeicon"), 0.2, {alpha:0}, "hinting");
        tl01.fromTo(__("swipeicon"), 0.6, {x:0}, {x:10, repeat:3, yoyo:true, ease:Back.easeInOut.config(1.2), repeatDelay:0.4}, "hinting+=0.6");
        tl01.to(__("swipeicon"), 0.6, {alpha:0}, "hinting+=6");
     
        tl01.to(__("cta"), 0.5, { scale:1.05, yoyo:true, repeat:1, ease:Power2.easeInOut, transformOrigin:"center center" });
            tl01.to(this, 0, { onComplete:rollover });

        tlRollover.to(__("cta"), 0.5, { scale:1.05, yoyo:true, repeat:1, ease:Power2.easeInOut, transformOrigin:"center center" }, "rollover");

        __("switch").onclick = function(){
            placePixel();
            if(__("switch").classList.contains("on")){
                __("switch").classList.add("off");
                __("switch").classList.remove("on");
                tlUspKoop.play(0);

                if(clickout == "koop"){                    
                    clickout = "huur";                    
                } else {
                    clickout = "koop";
                }
            } else {
                __("switch").classList.add("on");
                __("switch").classList.remove("off");
                tlUspHuur.play(0);

                if(clickout == "koop"){                    
                    clickout = "huur";                    
                } else {
                    clickout = "koop";
                }
            }
        };

        tlUspKoop.staggerTo(uspsToAnimate, 0.4, {x:-config.bannerWidth, ease:Power3.easeInOut}, 0.05, "switch");
        tlUspKoop.staggerTo(uspsToAnimateOut, 0.4, {x:0, ease:Power3.easeInOut}, 0.05, "switch+=0.4");
        tlUspKoop.to(switchtxtOut, 0.4, {alpha:0, x:10}, "switch");
        tlUspKoop.to(switchtxt, 0.4, {alpha:1, x:0}, "switch");

        tlUspHuur.staggerTo(uspsToAnimateOut, 0.4, {x:-config.bannerWidth, ease:Power3.easeInOut}, 0.05, "switch");
        tlUspHuur.staggerTo(uspsToAnimate, 0.4, {x:0, ease:Power3.easeInOut}, 0.05, "switch+=0.4");
        tlUspHuur.to(switchtxt, 0.4, {alpha:0, x:10}, "switch");
        tlUspHuur.to(switchtxtOut, 0.4, {alpha:1, x:0}, "switch");


        function rollover(){
            __("banner").onmouseover = function(){
                tlRollover.play(0);
            };

            __("banner").onmouseout = function(){
                
                tlRollover.reverse();
            };
        }

        // Custom pixel
        function placePixel(){
            if(!placedPixel){
                var axel = Math.random() + "";
                var a = axel * 10000000000000;
                var track = new Image();
                track.src = dd.pixel + a + '?';

                placedPixel = true;
            }
        }

        if (srBanner.debug) {
            if (srBanner.debug && srBanner.pauseFrom) {
                console.log("pause from " + srBanner.pauseFrom);
                tl01.pause(srBanner.pauseFrom);
            }

            if (srBanner.debug && srBanner.playFrom) {
                console.log("play from " + srBanner.playFrom);
                tl01.pause(srBanner.playFrom);
            }

            if (srBanner && srBanner.backupImage) {
                console.log("create backup images of last frame");
                tl01.add("screenshot");
                tl01.pause("screenshot");
            }
        }
    }

    if (callback) {
        callback();
    }
}

module.exports.animate = animate;