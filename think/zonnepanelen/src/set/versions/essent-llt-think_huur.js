function setData(callback) {

    var devDynamicContent = {};

    devDynamicContent.srFeed = [{}];
    devDynamicContent.srFeed[0]._id = 0;
    devDynamicContent.srFeed[0].isWorking = "Default feed 2";
    devDynamicContent.srFeed[0].exit_url = "";
    devDynamicContent.srFeed[0].versie = "huur";
    devDynamicContent.srFeed[0].copy = {};
    devDynamicContent.srFeed[0].copy.h1 = "<strong>Betaal ook véél minder</strong><br />voor energie"; 
    // devDynamicContent.srFeed[0].copy.h2 = "Met de zonnepanelen via Essent";
    devDynamicContent.srFeed[0].copy.koop_end_txt = "Zonnepanelen kopen via Essent? Daarom:";
    devDynamicContent.srFeed[0].copy.usp1_koop = "Advies op maat";
    devDynamicContent.srFeed[0].copy.usp2_koop = "Bespaar tot € 600,- per jaar";
    devDynamicContent.srFeed[0].copy.usp3_koop = "25 jaar rendementsgarantie";
    devDynamicContent.srFeed[0].copy.usp3_koop_sky = "25 jaar rendements-<br />garantie";
    devDynamicContent.srFeed[0].copy.huur_end_txt = "Zonnepanelen huren via Essent? Daarom:";
    devDynamicContent.srFeed[0].copy.usp1_huur = "Maandelijks opzegbaar";
    devDynamicContent.srFeed[0].copy.usp2_huur = "Binnen 1 dag geïnstalleerd";
    devDynamicContent.srFeed[0].copy.usp3_huur = "Inclusief service & onderhoud";
    devDynamicContent.srFeed[0].cta = "Doe de check";
    devDynamicContent.srFeed[0].cta_sky = "Doe de check";
    
    devDynamicContent.srFeed[0].background300x250_1 = dimension("bg-tel-300x250.jpg", "300x250");
    devDynamicContent.srFeed[0].background336x280_1 = dimension("bg-tel-336x280.jpg", "336x280");
    devDynamicContent.srFeed[0].background320x240_1 = dimension("bg-tel-320x240.jpg", "320x240");
    devDynamicContent.srFeed[0].background160x600_1 = dimension("bg-tel-160x600.jpg", "160x600");
    devDynamicContent.srFeed[0].background728x90_1 = dimension("bg-tel-728x90.jpg", "728x90");
    devDynamicContent.srFeed[0].background300x600_1 = dimension("bg-tel-300x600.jpg", "300x600");
    devDynamicContent.srFeed[0].background970x250_1 = dimension("bg-tel-970x250.jpg", "970x250");

    devDynamicContent.srFeed[0].pixel = 'https://ad.doubleclick.net/ddm/activity/src=4556414;type=engag0;cat=essen00-;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=';

    return devDynamicContent;
}

module.exports = setData;