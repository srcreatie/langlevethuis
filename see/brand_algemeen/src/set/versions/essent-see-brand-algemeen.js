function setData(callback) {

    var devDynamicContent = {};

    devDynamicContent.srFeed = [{}];
    devDynamicContent.srFeed[0]._id = 0;
    devDynamicContent.srFeed[0].isWorking = "Default feed 2";
    devDynamicContent.srFeed[0].exit_url = "";
    devDynamicContent.srFeed[0].copy = {};
    // devDynamicContent.srFeed[0].copy.title1 = "Thuis is de plek...";
    // devDynamicContent.srFeed[0].copy.title2 = "...waar je sport...";
    // devDynamicContent.srFeed[0].copy.title3 = "...waar je werkt...";
    devDynamicContent.srFeed[0].copy.title4a = "Thuis wordt steeds...";
    devDynamicContent.srFeed[0].copy.title4b = "makkelijker en leuker";

    devDynamicContent.srFeed[0].copy.textTopRectangle = 20;
    devDynamicContent.srFeed[0].bgImageName = "auto";

    devDynamicContent.srFeed[0].animationVersion = "brand_algemeen";

    devDynamicContent.srFeed[0].textPosition160x600 = { left: 0, top: 240 };
    devDynamicContent.srFeed[0].textPosition300x600 = { left: 18, bottom: 100 };
    devDynamicContent.srFeed[0].textPosition970x250 = { left: 18, bottom: 20 };
    devDynamicContent.srFeed[0].textPosition320x240 = { left: 18, top: 20 };
    devDynamicContent.srFeed[0].textPosition728x90 = { left: 335, centerY: 0 };
    devDynamicContent.srFeed[0].textPosition728x904b = { left: 335, top: 40 };


    devDynamicContent.srFeed[0].copy.cta_text = "Meer weten";
    devDynamicContent.srFeed[0].copy.cta_text_728 = "Meer weten";

    devDynamicContent.srFeed[0].position = {};
    devDynamicContent.srFeed[0].position.cta = { right: 15, bottom: 22 }
    devDynamicContent.srFeed[0].position.cta_970x250 = { el: __("footer"), centerX: 0, bottom: 22 }
    devDynamicContent.srFeed[0].position.cta_320x240 = { el: __("footer"), centerY: -2, right: 15 }
    devDynamicContent.srFeed[0].position.cta_160x600 = { el: __("footer"), centerX: 0, bottom: 70 }


    devDynamicContent.srFeed[0].color = {};
    devDynamicContent.srFeed[0].color.cta_bgcolor = "#00a9d1";
    devDynamicContent.srFeed[0].color.essentMain = "#E60167";

    devDynamicContent.srFeed[0].test = asset("auto.png");

    devDynamicContent.srFeed[0].background300x600_4 = dimension("bg-300x400-algmeen-4.jpg", "300x600");
    devDynamicContent.srFeed[0].background300x600_5 = dimension("bg-300x400-algmeen-5.jpg", "300x600");

    devDynamicContent.srFeed[0].background728x90_4 = dimension("bg-325x90-algmeen-4.jpg", "728x90");
    devDynamicContent.srFeed[0].background728x90_5 = dimension("bg-325x90-algmeen-5.jpg", "728x90");

    devDynamicContent.srFeed[0].background300x600_4 = dimension("bg-300x400-algmeen-4.jpg", "160x600");
    devDynamicContent.srFeed[0].background300x600_5 = dimension("bg-300x400-algmeen-5.jpg", "160x600");

    devDynamicContent.srFeed[0].background970x250_4 = dimension("bg-820x250-algmeen-4.jpg", "970x250");
    devDynamicContent.srFeed[0].background970x250_5 = dimension("bg-820x250-algmeen-5.jpg", "970x250");

    devDynamicContent.srFeed[0].background320x240_4 = dimension("bg-320x240-algmeen-4.jpg", "320x240");
    devDynamicContent.srFeed[0].background320x240_5 = dimension("bg-320x240-algmeen-5.jpg", "320x240");


    devDynamicContent.srFeed[0].background320x240_4 = dimension("bg-320x240-algmeen-4.jpg", "300x250");
    devDynamicContent.srFeed[0].background320x240_5 = dimension("bg-320x240-algmeen-5.jpg", "300x250");

    devDynamicContent.srFeed[0].background320x240_4 = dimension("bg-320x240-algmeen-4.jpg", "336x280");
    devDynamicContent.srFeed[0].background320x240_5 = dimension("bg-320x240-algmeen-5.jpg", "336x280");


    // devDynamicContent.srFeed[0].background300x250 = dimension("bg-auto-300x250.jpg", "300x250");
    // devDynamicContent.srFeed[0].background160x600 = dimension("bg-auto-160x600.jpg", "160x600");

    // devDynamicContent.srFeed[0].background250x250 = dimension("bg-auto-300x250.jpg", "250x250");
    // devDynamicContent.srFeed[0].background320x240 = dimension("bg-auto-300x250.jpg", "320x240");
    // devDynamicContent.srFeed[0].background728x90 = dimension("bg-auto-728x90.jpg", "728x90");
    // devDynamicContent.srFeed[0].background970x250 = dimension("bg-auto-970x250.jpg", "970x250");

    return devDynamicContent;
}

module.exports = setData;