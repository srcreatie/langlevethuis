function setElements(callback) {

    config = {};
    config.bannerWidth = 728;
    config.bannerHeight = 90;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('semibold.woff'),
            asset('bold.woff'),
        ], add);
    }

    function add() {
        var textPosition = {};
        textPosition.topLeft = { left: 14, top: 20 }
        textPosition.bottomLeft = { left: 18, bottom: 170 }
        textPosition.topRight = { top: 20, right: 14 }
        textPosition.bottomRight = { bottom: 110, right: 14 }

        ___("bg4").image(dd.background728x90_4, { width: 325, height: 90, wrap: true })
        ___("bg5").image(dd.background728x90_5, { width: 325, height: 90, wrap: true })

        ___("bgPink").style({ width: 970, height: 250, background: dd.color.essentMain })

        ___("text4a")
            .text(dd.copy.title4a, { color: "rgba(255, 255, 255,0)", fontSize: 21, webfont: "bold", autoSize: false, css: "width:210px; z-index:20", height: 53,  padding: {top:10, x:10, bottom:10} })
            .position(dd.textPosition728x90);

        ___("text4b")
            .text(dd.copy.title4b, { color: "rgba(255, 255, 255,0)", fontSize: 21, webfont: "bold", autoSize: false, css: "width:210px; z-index:20", height: 53,  padding: {top:5, x:10, bottom:10} })
            .position(dd.textPosition728x904b);          

        ___("footer")
            .style({ background: "#fff", height: config.bannerHeight, width: 160, css:"z-index:2" })
            .position({ bottom: 0, right: 0 })

        ___("cta")
            .text(dd.copy.cta_text_728, { color: "#FFFFFF", webfont: "bold", textAlign: "center", fontSize: 12, css:"z-index:11" })
            .style({ css: "background: " + dd.color.cta_bgcolor + ";" })
            .position( {right:10, bottom:10});

        ___("logo")
            .image(asset("logo.png"), { width: 109, height: 20, fit: true, align:"center right" })
            .position({ right: 10, top: 10, css:"z-index:20;" });

        ___("endGraphic")
            .image(asset("llt-280.png"), { width: 300, height: 75, fit: true, align:"center center" })
            .position({ centerX: -90, centerY: 0 });

        sr.loading.done(position);
    }

    function position() {
        callback();
        
    }
}

function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else {
            window.open(clickTag);
        }
    }
}

module.exports.setElements = setElements;