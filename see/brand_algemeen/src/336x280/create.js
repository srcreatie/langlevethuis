function setElements(callback) {

    config = {};
    config.bannerWidth = 336;
    config.bannerHeight = 280;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('semibold.woff'),
        asset('bold.woff'),
        ], add);
    }

    function add() {
        var textPosition = {};
        textPosition.topLeft = { left: 14, top: 20 }
        textPosition.bottomLeft = { left: 18, bottom: 170 }
        textPosition.topRight = { top: 20, right: 14 }
        textPosition.bottomRight = { bottom: 110, right: 14 }

        ___("bg4").image(dd.background320x240_4, { width: 336, height: 280, wrap: true })
        ___("bg5").image(dd.background320x240_5, { width: 336, height: 280, wrap: true })
        
        ___("bgPink").style({ width: 336, height: 280, background: dd.color.essentMain })

        ___("footer")
            .style({ background: "#fff", height: 65, width: config.bannerWidth, css: "z-index:10" })
            .position({ bottom: 0, left: 0 })

        ___("text4a")
            .text(dd.copy.title4a, { color: "rgba(255, 255, 255,0)", fontSize: 18, webfont: "bold", autoSize: false, css: "width:175px", height: 53, backgroundColor: dd.color.essentMain, padding: { top: 10, x: 10, bottom: 10 } })
            .position(dd.textPosition320x240);

        ___("text4b")
            .text(dd.copy.title4b, { color: "rgba(255, 255, 255,0)", fontSize: 18, webfont: "bold", autoSize: false, css: "width:175px", height: 53, backgroundColor: dd.color.essentMain, padding: { top: 5, x: 10, bottom: 10 } })
            .position({ el: __("text4a"), top: 30, left: 0 });

        ___("cta")
            .text(dd.copy.cta_text, { color: "#FFFFFF", webfont: "bold", textAlign: "center", fontSize: 12, css: "z-index:11" })
            .style({ css: "background: " + dd.color.cta_bgcolor + ";" })
            .position(dd.position.cta_320x240);

        ___("logo")
            .image(asset("logo.png"), { width: 120, height: 25, fit: true })
            .position({ left: 15, el: __("footer"), centerY: -2, css: "z-index:13" });

        ___("endGraphic")
            .image(asset("llt-280.png"), { width: 240, height: 85, fit: true, align: "center center" })
            .position({ centerX: 0, centerY: -40 });

        sr.loading.done(position);
    }

    function position() {
        callback();
    }
}


function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else {
            window.open(clickTag);
        }
    }
}

module.exports.setElements = setElements;