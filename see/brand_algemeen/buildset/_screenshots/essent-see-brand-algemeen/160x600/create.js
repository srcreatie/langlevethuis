function setElements(callback) {

    config = {};
    config.bannerWidth = 160;
    config.bannerHeight = 600;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('semibold.woff'),
            asset('bold.woff'),
        ], add);
    }

    function add() {
        var textPosition = {};
        textPosition.topLeft = { left: 14, top: 20 }
        textPosition.bottomLeft = { left: 18, bottom: 170 }
        textPosition.topRight = { top: 20, right: 14 }
        textPosition.bottomRight = { bottom: 110, right: 14 }
        
        ___("bg4").image(dd.background300x600_4, { width: 160, height: 240, wrap: true })        

        ___("bg5").image(dd.background300x600_5, { width: 160, height: 240, wrap: true })        

        ___("bgPink").style({ width: 300, height: 600, background: dd.color.essentMain })

        ___("footer")
            .style({ background: "#fff", height: 120, width: config.bannerWidth, css:"z-index:10"  })
            .position({ bottom: 0, left: 0 })

        ___("text4a")
            .text(dd.copy.title4a, { color: "rgba(255, 255, 255,1)", fontSize: 18, webfont: "bold", autoSize: false, width:160, height: 230, backgroundColor: dd.color.essentMain, padding: {top:30, x:10, bottom:10} }).style({height:230, css:"z-index:5"})
            .position(dd.textPosition160x600);

        ___("text4b")
            .text(dd.copy.title4b, { color: "rgba(255, 255, 255,1)", fontSize: 18, webfont: "bold", autoSize: false, width:160, height: 230, padding: {top:80, x:10, bottom:10} }).style({height:230, css:"z-index:6"})
            .position(dd.textPosition160x600);          

        ___("cta")
            .text(dd.copy.cta_text, { color: "#FFFFFF", webfont: "bold", textAlign: "center", fontSize: 12 , css:"z-index:11" })
            .style({ css: "background: " + dd.color.cta_bgcolor + ";" })
            .position( dd.position.cta_160x600 );

        ___("logo")
            .image(asset("logo.png"), { width: 120, height: 30, fit: true, align:"center center"  })
            .position({ centerX: 0, bottom: 20 , css:"z-index:13" });

        ___("endGraphic")
            .image(asset("llt-140.png"), { width: 140, height: 106, fit: true, align:"center center" })
            .position({ centerX: 0, centerY: -40 });

        sr.loading.done(position);
    }

    function position() {
        callback();
    }
}

function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else {
            window.open(clickTag);
        }
    }
}

module.exports.setElements = setElements;