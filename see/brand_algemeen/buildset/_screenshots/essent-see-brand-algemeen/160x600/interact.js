function animate(callback) {

    setAnimation();

    function setAnimation() {
        __("creative").style.opacity = 1;

        var tl01 = new TimelineMax();
 
        tl01.to(bg5, 0, { alpha: 0 }, 0)

        tl01.to(__("bg4"), 0.6, { alpha: 1 }, 'changeTobg4');
        tl01.to(__("text4a"), 1, { alpha: 1 }, 'changeTobg4')
            
            tl01.to(this, 1, {});

        tl01.to(__("bg4"), 0.6, { alpha: 0 }, 'same')
        tl01.to(__("bg5"), 0.6, { alpha: 1 }, 'same')

        tl01.from(__("text4b"), 1, { alpha: 0 }, 'changeTobg4+1')

            tl01.to(this, 2, {})

        tl01.to(__(["text4a", "text4b"]), 0.3, { alpha: 0 }, "toend")
        tl01.from(__("bgPink"), 1, { height: 0, transformOrigin:"bottom center" }, "toend")

        tl01.from(__("endGraphic"), 0.6, { alpha: 0 }, "toend+=0.2")

        if (srBanner.debug) {
            if (srBanner.debug && srBanner.pauseFrom) {
                console.log("pause from " + srBanner.pauseFrom);
                tl01.pause(srBanner.pauseFrom);
            }

            if (srBanner.debug && srBanner.playFrom) {
                console.log("play from " + srBanner.playFrom);
                tl01.pause(srBanner.playFrom);
            }

            if (srBanner && srBanner.backupImage) {
                console.log("create backup images of last frame");
                tl01.add("screenshot");
                tl01.pause("screenshot");
            }
        }
    }

    if (callback) {
        callback();
    }
}

module.exports.animate = animate;