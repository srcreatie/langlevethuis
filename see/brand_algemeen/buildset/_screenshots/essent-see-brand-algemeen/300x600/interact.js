function animate(callback) {

    setAnimation();

    function setAnimation() {
        __("creative").style.opacity = 1;

        var tl01 = new TimelineMax();

        tl01.to(bg5, 0, { alpha: 0 }, 0)
        
        tl01.to(__("bg4"), 0.6, { alpha: 1 }, 'changeTobg4');

        tl01.from(__("text4a"), 1, { x: -100, scaleX: 0, ease: Back.easeOut, transformOrigin: "center left" })
        tl01.to(__("text4a"), 1, { color: "rgba(255, 255, 255, 1)" })

            tl01.to(this, 1, {});

        tl01.to(__("bg4"), 0.6, { alpha: 0 }, 'same')
        tl01.to(__("bg5"), 0.6, { alpha: 1 }, 'same')
        tl01.to(__("text4a"), 1, { y: -30, ease: Back.easeOut, transformOrigin: "center left" }, "same")
        tl01.from(__("text4b"), 1, { x: -100, scaleX: 0, ease: Back.easeOut, transformOrigin: "center left" }, "same")
        
        tl01.to(__("text4b"), 1, { color: "rgba(255, 255, 255, 1)" })

            tl01.to(this, 1, {})

        tl01.to(__(["text4a", "text4b"]), 0.3, { color: "rgba(255, 255, 255, 0)" }, "toend")
        tl01.to(__(["text4a", "text4b"]), 0.6, { x: 0, scaleX: 0, transformOrigin: "center left" }, "toend")
        tl01.from(__("bgPink"), 1, { width: 0, height: 0 }, "toend")

        tl01.from(__("endGraphic"), 0.6, { alpha: 0 }, "toend+=0.6")

        if (srBanner.debug) {
            if (srBanner.debug && srBanner.pauseFrom) {
                console.log("pause from " + srBanner.pauseFrom);
                tl01.pause(srBanner.pauseFrom);
            }

            if (srBanner.debug && srBanner.playFrom) {
                console.log("play from " + srBanner.playFrom);
                tl01.pause(srBanner.playFrom);
            }

            if (srBanner && srBanner.backupImage) {
                console.log("create backup images of last frame");
                tl01.add("screenshot");
                tl01.pause("screenshot");
            }
        }
    }

    if (callback) {
        callback();
    }
}

module.exports.animate = animate;