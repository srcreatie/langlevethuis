function setData(callback) {

    var devDynamicContent = {};

    devDynamicContent.srFeed = [{}];
    devDynamicContent.srFeed[0]._id = 0;
    devDynamicContent.srFeed[0].isWorking = "Default feed 2";
    devDynamicContent.srFeed[0].exit_url = "";
    devDynamicContent.srFeed[0].copy = {};
    devDynamicContent.srFeed[0].copy.title1 = "Schnabbelen op de braderie…";
    devDynamicContent.srFeed[0].copy.title2 = "én bijverdienen met je zonnepanelen?";
    devDynamicContent.srFeed[0].copy.title3 = "Thuis wordt steeds voordeliger";
    
    devDynamicContent.srFeed[0].copy.textTopRectangle = 20;
    devDynamicContent.srFeed[0].bgImageName = "auto";

    devDynamicContent.srFeed[0].textPosition160x600 = { left: 0, top: 240 };
    devDynamicContent.srFeed[0].textPosition300x600 = { left: 18, bottom: 100 };
    devDynamicContent.srFeed[0].textPosition970x250 = { left: 18, bottom: 20 };
    devDynamicContent.srFeed[0].textPosition320x240 = { left: 18, top: 20 };
    devDynamicContent.srFeed[0].textPosition728x90 = { left: 335, centerY: 0 };
    devDynamicContent.srFeed[0].textPosition728x904b = { left: 335, top: 40 };

    devDynamicContent.srFeed[0].copy.cta_text = "Meer over<br>Zonnepanelen";
    devDynamicContent.srFeed[0].copy.cta_text_728 = "Meer over Zonnepanelen";
    devDynamicContent.srFeed[0].position = {};
    devDynamicContent.srFeed[0].position.cta = { right: 15, bottom: 15 }
    devDynamicContent.srFeed[0].position.cta_970x250 = { el: __("footer"), centerX: 0, bottom: 22 }
    devDynamicContent.srFeed[0].position.cta_320x240 = { el: __("footer"), centerY: -2, right: 15 }
    devDynamicContent.srFeed[0].position.cta_160x600 = { el: __("footer"), centerX: 0, bottom: 60 }

    devDynamicContent.srFeed[0].color = {};
    devDynamicContent.srFeed[0].color.cta_bgcolor = "#00a9d1";
    devDynamicContent.srFeed[0].color.essentMain = "#E60167";

    devDynamicContent.srFeed[0].test = asset("auto.png");

    devDynamicContent.srFeed[0].background300x600_1 = dimension("bg-300x520-zon-algemeen-1.jpg", "300x600");
    devDynamicContent.srFeed[0].background300x600_2 = dimension("bg-300x520-zon-algemeen-2.jpg", "300x600");

    devDynamicContent.srFeed[0].background728x90_1 = dimension("bg-325x90-zon-algemeen-1.jpg", "728x90");
    devDynamicContent.srFeed[0].background728x90_2 = dimension("bg-325x90-zon-algemeen-2.jpg", "728x90");

    devDynamicContent.srFeed[0].background300x600_1 = dimension("bg-300x520-zon-algemeen-1.jpg", "160x600");
    devDynamicContent.srFeed[0].background300x600_2 = dimension("bg-300x520-zon-algemeen-2.jpg", "160x600");

    devDynamicContent.srFeed[0].background970x250_1 = dimension("bg-820x250-zon-algemeen-1.jpg", "970x250");
    devDynamicContent.srFeed[0].background970x250_2 = dimension("bg-820x250-zon-algemeen-2.jpg", "970x250");

    devDynamicContent.srFeed[0].background320x240_1 = dimension("bg-320x240-zon-algemeen-1.jpg", "320x240");
    devDynamicContent.srFeed[0].background320x240_2 = dimension("bg-320x240-zon-algemeen-2.jpg", "320x240");

    devDynamicContent.srFeed[0].background320x240_1 = dimension("bg-320x240-zon-algemeen-1.jpg", "300x250");
    devDynamicContent.srFeed[0].background320x240_2 = dimension("bg-320x240-zon-algemeen-2.jpg", "300x250");

    devDynamicContent.srFeed[0].background320x240_1 = dimension("bg-320x240-zon-algemeen-1.jpg", "336x280");
    devDynamicContent.srFeed[0].background320x240_2 = dimension("bg-320x240-zon-algemeen-2.jpg", "336x280");

    return devDynamicContent;
}

module.exports = setData;