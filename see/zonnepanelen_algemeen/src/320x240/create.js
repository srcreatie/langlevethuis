function setElements(callback) {

    config = {};
    config.bannerWidth = 320;
    config.bannerHeight = 240;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('semibold.woff'),
            asset('bold.woff'),
        ], add);
    }

    function add() {
        var textPosition = {};
        textPosition.topLeft = { left: 14, top: 20 }
        textPosition.bottomLeft = { left: 18, bottom: 170 }
        textPosition.topRight = { top: 20, right: 14 }
        textPosition.bottomRight = { bottom: 110, right: 14 }

        ___("bg1").image(dd.background320x240_1, { width: 320, height: 240, wrap: true })
        ___("bg2").image(dd.background320x240_2, { width: 320, height: 240, wrap: true })
        
        ___("footer")
            .style({ background: "#fff", height: 65, width: config.bannerWidth, css:"z-index:10"  })
            .position({ bottom: 0, left: 0 })

        ___("text1")
            .text(dd.copy.title1, { color: "rgba(255, 255, 255,0)", fontSize: 18, webfont: "bold", autoSize: true, css: "width:205px", height: 53, backgroundColor: dd.color.essentMain, padding: 10 })
            .position(dd.textPosition320x240);

        ___("text2")
            .text(dd.copy.title2, { color: "rgba(255, 255, 255,0)", fontSize: 18, webfont: "bold", autoSize: true, css: "width:205px", height: 53, backgroundColor: dd.color.essentMain, padding: 10 })
            .position(dd.textPosition320x240);

        ___("text3")
            .text(dd.copy.title3, { color: "rgba(255, 255, 255,0)", fontSize: 18, webfont: "bold", autoSize: true, css: "width:205px", height: 53, backgroundColor: dd.color.essentMain, padding: 10 })
            .position(dd.textPosition320x240);
            
        ___("cta")
            .text(dd.copy.cta_text, { color: "#FFFFFF", webfont: "bold", textAlign: "center", fontSize: 12 , css:"z-index:11" })
            .style({ css: "background: " + dd.color.cta_bgcolor + ";" })
            .position( dd.position.cta_320x240); 

        ___("logo")
            .image(asset("logo.png"), { width: 120, height: 25, fit: true  })
            .position({ left: 15, el:__("footer"), centerY: -2, css:"z-index:13" });

        sr.loading.done(position);
    }

    function position() {
        callback();
    }
}


function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else {
            window.open(clickTag);
        }
    }
}

module.exports.setElements = setElements;