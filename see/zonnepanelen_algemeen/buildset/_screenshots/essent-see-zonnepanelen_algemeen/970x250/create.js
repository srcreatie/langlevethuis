function setElements(callback) {

    config = {};
    config.bannerWidth = 970;
    config.bannerHeight = 250;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('semibold.woff'),
            asset('bold.woff'),
        ], add);
    }

    function add() {
        var textPosition = {};
        textPosition.topLeft = { left: 14, top: 20 }
        textPosition.bottomLeft = { left: 18, bottom: 170 }
        textPosition.topRight = { top: 20, right: 14 }
        textPosition.bottomRight = { bottom: 110, right: 14 }

        ___("bg1").image(dd.background970x250_1, { width: 820, height: 250, wrap: true })
        ___("bg2").image(dd.background970x250_2, { width: 820, height: 250, wrap: true })
       
        ___("text1")
            .text(dd.copy.title1, { color: "rgba(255, 255, 255,0)", fontSize: 28, webfont: "bold", autoSize: true, css: "width:245px", height: 53, backgroundColor: dd.color.essentMain, padding: 10 })
            .position(dd.textPosition970x250);

        ___("text2")
            .text(dd.copy.title2, { color: "rgba(255, 255, 255,0)", fontSize: 28, webfont: "bold", autoSize: true, css: "width:245px", height: 53, backgroundColor: dd.color.essentMain, padding: 10 })
            .position(dd.textPosition970x250);

        ___("text3")
            .text(dd.copy.title3, { color: "rgba(255, 255, 255,0)", fontSize: 28, webfont: "bold", autoSize: true, css: "width:245px", height: 53, backgroundColor: dd.color.essentMain, padding: 10 })
            .position(dd.textPosition970x250);
     
        ___("footer")
            .style({ background: "#fff", height: config.bannerHeight, width: 175, css:"z-index:10" })
            .position({ bottom: 0, right: 0 })

        ___("cta")
            .text(dd.copy.cta_text, { color: "#FFFFFF", webfont: "bold", textAlign: "center", fontSize: 12, css:"z-index:11" })
            .style({ css: "background: " + dd.color.cta_bgcolor + ";" })
            .position( dd.position.cta_970x250 );

        ___("logo")
            .image(asset("logo.png"), { width: 109, height: 25, fit: true })
            .position({ el: __("footer"), right: 20, top: 20, css:"z-index:20;" });

        sr.loading.done(position);
    }

    function position() {
        callback();
    }
}


function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else {
            window.open(clickTag);
        }
    }
}

module.exports.setElements = setElements;