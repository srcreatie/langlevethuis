function animate(callback) {

    setAnimation();

    function setAnimation() {
        __("creative").style.opacity = 1;

        var tl01 = new TimelineMax();

        tl01.to(bg2, 0, { alpha: 0 }, 0)

        tl01.from(__("text1"), 1, { x: -100, scaleX: 0, ease: Back.easeOut, transformOrigin: "center left" })
        tl01.to(__("text1"), 1, { color: "rgba(255, 255, 255, 1)" })

            tl01.to(this, 1, {})

        tl01.to(__("text1"), 0.3, { color: "rgba(255, 255, 255, 0)" })
        tl01.to(__("text1"), 0.6, { x: -100, scaleX: 0, transformOrigin: "center left" })

        tl01.to(__("bg1"), 0.6, { alpha: 0 }, 'changeTobg2')
        tl01.to(__("bg2"), 0.6, { alpha: 1 }, 'changeTobg2')

        tl01.from(__("text2"), 1, { x: -100, scaleX: 0, ease: Back.easeOut, transformOrigin: "center left" })
        tl01.to(__("text2"), 1, { color: "rgba(255, 255, 255, 1)" })
    
            tl01.to(this, 1, {})

        tl01.to(__("text2"), 0.3, { color: "rgba(255, 255, 255, 0)" })
        tl01.to(__("text2"), 0.6, { x: 0, scaleX: 0, transformOrigin: "center left" })

            tl01.to(this, 0.5, {});
    
        tl01.from(__("text3"), 1, { x: -100, scaleX: 0, ease: Back.easeOut, transformOrigin: "center left" })
        tl01.to(__("text3"), 1, { color: "rgba(255, 255, 255, 1)" })

        if (srBanner.debug) {
            if (srBanner.debug && srBanner.pauseFrom) {
                console.log("pause from " + srBanner.pauseFrom);
                tl01.pause(srBanner.pauseFrom);
            }

            if (srBanner.debug && srBanner.playFrom) {
                console.log("play from " + srBanner.playFrom);
                tl01.pause(srBanner.playFrom);
            }

            if (srBanner && srBanner.backupImage) {
                console.log("create backup images of last frame");
                tl01.add("screenshot");
                tl01.pause("screenshot");
            }
        }
    }

    if (callback) {
        callback();
    }
}

module.exports.animate = animate;