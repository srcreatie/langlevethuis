module.exports = function(grunt) {

    console.log('testing');

    const path = require('path');



    //used to name the zip file
    grunt.option('currentFolder', process.env.PWD.match(/([^\/]*)\/*$/)[1]);

    grunt.option("destination", '../../buildset/preview/' + grunt.option('feed') + '/' + grunt.option('currentFolder'));
    grunt.option("destinationSS", '../../buildset/_screenshots/' + grunt.option('feed') + '/' + grunt.option('currentFolder'));

    grunt.initConfig({



        copy: {
            options: {
                processContentExclude: ['**/*.{png,gif,jpg,ico,psd}']
            },

            buildset: {
                files: [{
                    cwd: './',
                    src: ['**'],
                    dest: grunt.option('destination'),
                    expand: true
                }]
            },
            buildsetSS: {
                files: [{
                    cwd: './',
                    src: ['**'],
                    dest: grunt.option('destinationSS'),
                    expand: true
                }]
            },
            assetFolderSet: {

                files: [{
                        cwd: '../set/assets/',
                        src: '<%= assetfiles %>',
                        dest: grunt.option('destination'),
                        expand: true,
                    }


                ]
            },
            assetFolderSetSS: {

                files: [{
                    cwd: '../set/assets/',
                    src: '<%= assetfiles %>',
                    dest: grunt.option('destinationSS'),
                    expand: true,
                }]
            },
            previewPage: {

                files: [{
                    cwd: './',
                    src: ['index.php', 'srPreview.json'],
                    dest: '../buildset/preview/',
                    expand: true,
                }]

            }
        },



        webpack: {
            options: {
                resolve: {
                    alias: {
                        feedType: path.resolve(__dirname, "src/set/" + (grunt.option('feedType') || "srData") + ".js"),
                        feed: path.resolve(__dirname, "src/set/versions/" + (grunt.option('feed') || "versie1") + ".js"),
                    }
                }
            },
            working: {
                entry: "./sr-main.js",

                output: {
                    path: path.resolve(__dirname, process.env.PWD),
                    filename: grunt.option('folder') + "/srbundle.js",
                    publicPath: "assets"
                },

            },
            publishset: {
                entry: "./sr-main.js",
                output: {
                    path: path.resolve(__dirname, process.env.PWD),
                    filename: "srbundle.js",
                    publicPath: "assets",

                },

            },
        },

        clean: {
            options: { force: true },
            buildSet: [grunt.option('destination') + '/*.js',
                grunt.option('destination') + '/_template.html',
                grunt.option('destination') + '/*.map',
                '!' + grunt.option('destination') + '/srbundle.js',
                '!' + grunt.option('destination') + '/srbundle-min.js',
                '_screenshots'
            ],
        },

        watch: {
            scripts: {
                files: [
                    './**/*.js',
                    './**/*.**',
                    '!./**/*bundle.js',
                ],
                tasks: [], //tasks are selected in onwatch function (bottom of file because of dedpendence of the entry folder)
                options: {
                    spawn: false,
                    livereload: true,
                }
            }
        },

        compress: {
            buildset: {
                options: {
                    archive: '../../buildset/deliverables/' + grunt.option('feed') + '/' + grunt.option('feed') + '_' + grunt.option('currentFolder') + '.zip',
                    mode: 'zip'

                },
                files: [
                    { cwd: grunt.option('destination'), expand: true, src: ['**'] }
                ]
            }
        },

        run: {
            tool: {
                cmd: './updateall.sh',
            }
        },

        replace: {
            previewPageSubfolders: {
                src: ["../buildset/preview/srPreview.json"],
                expand: true,
                overwrite: true,
                replacements: [{
                    from: '"hasSubfolders": false,',
                    to: '"hasSubfolders": true,',
                }]
            },

            publisherDcs: {
                src: ["**/index.html"],
                expand: true,
                overwrite: true,
                replacements: [{
                        from: '<!--studio-->',
                        to: '<script src="https://s0.2mdn.net/ads/studio/Enabler.js"></script>',
                    },
                    {
                        // from: '<script src="../lib/GSDevTools.js"></script>',
                        // to: '////GSDevTools'
                    }
                ]
            },
            publisherDcm: {
                src: ["**/index.html"],
                expand: true,
                overwrite: true,
                replacements: [{
                    from: '<script src="https://s0.2mdn.net/ads/studio/Enabler.js"></script>',
                    to: '<!--studio-->',
                }, {
                    // from: '<script src="../lib/GSDevTools.js"></script>',
                    // to: ''
                }]
            },

            assetFolderSet: {
                src: [grunt.option('destination') + "/*.js", grunt.option('destination') + "/*.html"],
                expand: true,
                overwrite: true,
                replacements: [{
                    from: 'srBanner.isLocal = true;',
                    to: 'srBanner.isLocal = false;',

                }, {
                    from: '"debug":true',
                    to: '"debug":false'
                }, {
                    from: '<script src="../lib/GSDevTools.js"></script>',
                    to: ''
                }]

            },


            screenshotSet: {
                src: [grunt.option('destinationSS') + "/*.js"],
                expand: true,
                overwrite: true,
                replacements: [{
                        from: 'srBanner.isLocal = true;',
                        to: 'srBanner.isLocal = false;'
                    },
                    {
                        from: '"backupImage":false',
                        to: '"backupImage":true'

                    }, {
                        from: '<script src="../lib/GSDevTools.js"></script>',
                        to: ''
                    }
                ]

            },

        },
        sr: {
            getFiles: {

                options: {

                    files: [{
                        src: [
                            "../set/versions/" + grunt.option('feed') + ".js",
                            "./create.js"
                            // "../set/scripts/dcsSetup.js"
                        ]

                    }],
                    directory: '../../buildset/',
                    currentFolder: grunt.option('currentFolder')

                }
            }

        },
        processhtml: {
            options: {
                // Task-specific options go here. 
            },
            build: {
                files: {
                    './index.html': ['./index.html']
                }
                // Target-specific file lists and/or options go here. 
            },
        },

    });




    grunt.event.on('watch', function(action, filepath) {



        manifest = grunt.file.readJSON('./set/manifest.json');

        var testing = manifest.data.feed;

        grunt.config('webpack.options.resolve.alias.feed', path.resolve(__dirname, "src/set/versions/" + testing));

        if (manifest.data.dcs == true) {

            grunt.config('webpack.options.resolve.alias.feedType', path.resolve(__dirname, "src/set/dcsData.js"));

        } else {
            grunt.config('webpack.options.resolve.alias.feedType', path.resolve(__dirname, "src/set/srData.js"));

        }

        var e = filepath;
        e = e.split('/'); //break the string into an array

        if (e[e.length - 1] == "srbundle.js") {

            grunt.fail.fatal("dont publish if srbundle");
        }

        e.pop(); //remove its last element
        e = e.join('/');

        grunt.config('webpack.working.entry', "./" + e + "/sr-main.js");

        grunt.config('webpack.working.output.path', process.env.PWD);
        grunt.config('webpack.working.output.filename', "./" + e + "/srbundle.js");

        if (e.substring(0, 3) == "set") {

            grunt.fail.warn("set has changed, dont't create bundle")
            grunt.task.clearQueue()
            grunt.task.run(["run"]);
        } else {

            grunt.task.run(["webpack:working"]);

        }

    });



    grunt.config.dataFile = grunt.option('datafile');

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-run');
    grunt.loadNpmTasks('grunt-text-replace');
    grunt.loadNpmTasks('../grunt-sr');
    grunt.loadNpmTasks('grunt-webpack');
    grunt.loadNpmTasks('grunt-processhtml');

    grunt.file.setBase(process.env.PWD);

    grunt.registerTask('default', ['webpack:publishset']);
    grunt.registerTask('update', ['webpack:publishset']);
    grunt.registerTask('testing', ['processhtml:build']);
    // grunt.registerTask('testing', ['webpack:working']);
    grunt.registerTask('screenshot', ['webpack:publishset', 'copy:buildsetSS', 'sr', 'copy:assetFolderSetSS', 'replace:screenshotSet']);
    grunt.registerTask('new', ['webpack:publishset', 'copy:buildset', 'sr', 'copy:assetFolderSet', 'replace:assetFolderSet', 'clean:buildSet', 'compress:buildset'])
    grunt.registerTask('add_preview', ['copy:previewPage', 'replace:previewPageSubfolders'])
    grunt.registerTask('update_from_set', 'Update the complete set', function(name, val) {
        manifest = grunt.file.readJSON('../set/manifest.json');
        var feed_file = manifest.data.feed;

        grunt.config('webpack.options.resolve.alias.feed', path.resolve(__dirname, "src/set/versions/" + feed_file));

        var e = process.env.PWD;
        e = e.split('/'); //break the string into an array

        if (e[e.length - 1] == "srbundle.js") {

            grunt.fail.fatal("dont publish if srbundle");
        }

        e = e.join('/');

        grunt.config('webpack.working.entry', "./sr-main.js");
        grunt.config('webpack.working.output.path', process.env.PWD);
        grunt.config('webpack.working.output.filename', "./srbundle.js");

        grunt.task.run(["webpack:working"]);

        var publisherDCS = manifest.data.dcs;

        if (publisherDCS) {
            grunt.task.run(['replace:publisherDcs']);
        } else {

            grunt.task.run(['replace:publisherDcm']);
        }

    });

};